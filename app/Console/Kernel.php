<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //Commands\SendEmailsandSMS::class,
        Commands\ReportNotification::class,
        Commands\SendReminderSms::class,
        Commands\SendReminderToHiredContractors::class,
        Commands\SendReminderSmsFromScheduler::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /*$schedule->command('command:sendemailandsms')
        ->withoutOverlapping()
        ->everyMinute();*/

        $schedule->command('command:send_report_notification')
            ->withoutOverlapping()
            ->timezone('America/Chicago')
            ->dailyAt('20:00');

        $schedule->command('command:send_reminder_sms')
            ->withoutOverlapping();

        $schedule->command('command:send_reminder_message_to_hired_contractors')
            ->hourly();
        $schedule->command('command:Send_Reminder_Sms_From_Scheduler')
            ->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
