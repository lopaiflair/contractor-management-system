<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SendEmailsandSMS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:sendemailandsms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Email & SMS after time specified in settings.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            \Log::info("Cron Started : sendemailandsms");
            $addresses = \App\Address::select('id', 'added_on', 'mail', 'sms', 'email', 'phone_no', 'first_name')
                ->whereNotNull('email')
                ->whereNotNull('phone_no')
                ->where(function ($q) {
                    $q->where('sms', 'Not Yet')
                        ->orWhere('mail', 'Not Yet');
                })
                ->limit(1)
                ->orderBy('id', 'desc')
                ->get();

            $setting = \App\Setting::select('email_content', 'sms_content', 'send_email_after_days', 'send_email_after_hours', 'send_email_after_minutes', 'send_sms_after_days', 'send_sms_after_hours', 'send_sms_after_minutes')->where('id', 1)->first();
            $email_content = $setting->email_content;
            $sms_content = $setting->sms_content;

            $seconds_email = $seconds_sms = [];
            if (!empty($setting->send_email_after_days)) {
                $seconds_email[] = strtotime($setting->send_email_after_days . " days", 0);
            }
            if (!empty($setting->send_email_after_hours)) {
                $seconds_email[] = strtotime($setting->send_email_after_hours . " hours", 0);
            }
            if (!empty($setting->send_email_after_minutes)) {
                $seconds_email[] = strtotime($setting->send_email_after_minutes . " minutes", 0);
            }
            $timetoleft_email = array_sum($seconds_email);

            if (!empty($setting->send_sms_after_days)) {
                $seconds_sms[] = strtotime($setting->send_sms_after_days . " days", 0);
            }
            if (!empty($setting->send_sms_after_hours)) {
                $seconds_sms[] = strtotime($setting->send_sms_after_hours . " hours", 0);
            }
            if (!empty($setting->send_email_after_minutes)) {
                $seconds_sms[] = strtotime($setting->send_sms_after_minutes . " minutes", 0);
            }
            $timetoleft_sms = array_sum($seconds_sms);

            $current = strtotime("now");
            foreach ($addresses as $single) {
                $data = [];
                if (!empty($single->added_on)) {
                    $added_on = strtotime($single->added_on);
                    $diff = $current - $added_on;
                    if ($diff >= $timetoleft_email) {
                        if ($single->mail == "Not Yet" && !empty($single->email)) {
                            $mail = \Mail::send([], [], function ($message) use ($single, $email_content) {
                                $message->from('projects@3rdwave-marketing.com', 'Faith Properties Lead Generation')
                                    ->to($single->email, $single->first_name)
                                    ->subject("Address added to database")
                                    ->setBody($email_content, 'text/html');
                            });
                            $data['mail'] = "Sent";
                            $data['email_sent_on'] = now();
                        }
                    }
                    if ($diff >= $timetoleft_sms) {
                        if ($single->sms == "Not Yet" && !empty($single->phone_no)) {
                            $info = \Nexmo::message()->send([
                                'to' => $single->phone_no,
                                'from' => "18162276099",
                                'text' => strip_tags($sms_content),
                            ]);
                            $data['sms'] = "Sent";
                            $data['sms_sent_on'] = now();
                        }
                    }
                    if (!empty($data)) {
                        \App\Address::where('id', $single->id)->update($data);
                    }
                }
                \Log::info("Sent Email & SMS to Address ID - " . $single->id);
            }
            \Log::info("Cron Finished");
        } catch (\Exception $e) {
            \Log::error('Cron failed due to.' . $e->getMessage());
        }
    }
}
