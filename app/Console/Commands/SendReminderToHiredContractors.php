<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \Carbon\Carbon;

class SendReminderToHiredContractors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_reminder_message_to_hired_contractors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminder message to hired contractors.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $getHiredContractors = \App\Bookslot::where('hired', 'yes')->where('reminder', 'on')->whereDate('schedule_date', '<=', now())->get();
        if(!empty($getHiredContractors)){
            try{
                foreach($getHiredContractors as $user){
                    $current_datetime = Carbon::now()->format('Y-m-d H:i:s');
                    $date = $user->reminder_sent_on;
                    $newDate = "";
                    if($user->reminder_interval == 'hourly'){
                        $newDate = Carbon::parse($date)->addHour();
                    } else if ($user->reminder_interval == 'daily'){
                        $newDate = Carbon::parse($date)->addHour(24);
                    }
                    if($current_datetime >= $newDate){
                        \Telnyx\Telnyx::setApiKey(env('TELNYX_KEY'));
                        $info = \Telnyx\Message::Create([
                            'messaging_profile_id' => env('MESSAGE_PROFILE_ID'),
                            // 'from' => '+18162083113',
                            'to'   => $user->sent_text_message_to,
                            'text' => (!empty($user->reminder_message)) ? ("Hello $user->name,this is a reminder to meet Faith Properties at $user->timeslot, property location $user->what_address_you_are_coming_to ". $user->reminder_message) : ("Hello $user->name, this is a reminder to meet Faith Properties at $user->timeslot, property location $user->what_address_you_are_coming_to"),
                        ]);
                        if($info){
                            \App\Bookslot::where('id', $user->id)->update(['reminder_sent_on' => now()]);
                        }
                    }
                }
            } catch (\Exception $e){
                echo $e->getMessage();
            }
        }
    }
}
