<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \Carbon\Carbon;

class ReportNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_report_notification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Report notification to defined numbers in setting how many Email & SMS sent';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $date = Carbon::now();
            \Log::info($date->toRfc850String() . " Cron Started : send_report_notification");
            $setting = \App\Setting::where('id', 1)->first();
            if ($setting->report_notification == "On" && !empty($setting->ph_numbers_to_send_notification) && !empty($setting->report_notification_content)) {
                $to_send = array_map("trim", explode(',', $setting->ph_numbers_to_send_notification));

                $sms_count = \App\Address::where('sms', 'Sent')->where(\DB::raw('date(sms_sent_on)'), \Carbon\Carbon::today())->count();

                $email_count = \App\Address::where('mail', 'Sent')->where(\DB::raw('date(email_sent_on)'), \Carbon\Carbon::today())->count();

                $body = $setting->report_notification_content;
                $body = str_replace("{EMAIL_COUNT}", $email_count, $body);
                $body = str_replace("{SMS_COUNT}", $sms_count, $body);

                foreach ($to_send as $value) {
                    try {
                        $info = \Nexmo::message()->send([
                            'to' => $value,
                            'from' => "18162276099",
                            'text' => strip_tags($body),
                        ]);
                        \Log::info("Message Sent over " . $value);
                    } catch (\Exception $e) {
                        \Log::error('message sending over ' . $value . ' failed due to ' . $e->getMessage());
                    }
                }
            } else if ($setting->report_notification == "Off") {
                \Log::info("Report notification service is OFF");
            }
            \Log::info("Cron Finished");
        } catch (\Exception $e) {
            \Log::error('Cron failed due to.' . $e->getMessage());
        }
    }
}
