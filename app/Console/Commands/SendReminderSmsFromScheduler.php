<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \Carbon\Carbon;
use Telnyx;
use App\Skill;

class SendReminderSmsFromScheduler extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:Send_Reminder_Sms_From_Scheduler';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the reminder message before 30 minutes from Scheduler';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->time_formet = array(
            "10:00-10:30" => "10:00 am – 10:30 am",
            "10:45-11:15" => "10:45 am – 11:15 am",
            "11:45-12:15" => "11:45 am – 12:15 pm",
            "12:30-13:00" => "12:30 pm – 1:00 pm",
            "13:15-13:45" => "1:15 pm – 1:45 pm",
             
        );
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $sendReminderSms = \App\Scheduler::select('*')->where('reminder_sent_on', null)->get();
         
        if(!empty($sendReminderSms)){
            try{
                foreach($sendReminderSms as $user){
                    $timeslot = $user->timeslot;
                    $time = $user->schedule_date;
                    $proper_time = explode("-", $timeslot);
                    $combine_datetime = "$time $proper_time[0]";
                    $date = date("Y-m-d H:i");
                    $time = strtotime($combine_datetime);
                    $time = $time - (30 * 60);
                    $number="8168067811";
                    $combine_datetime = date("Y-m-d H:i:s", $time);
                    $current_datetime = Carbon::now()->format('Y-m-d H:i:s');

                    $timeformet_arrray=$this->time_formet;
 
                    $time_formet=$timeformet_arrray[$user->timeslot];
 
                    $skill_arr=explode (",", $user->skill_set);
                    $skill_names =  \App\Skill::select('skill')
                    ->whereIn('id', $skill_arr)
                    ->pluck('skill')->implode(', ');
                      if($current_datetime >= $combine_datetime){
                         \Telnyx\Telnyx::setApiKey(env('TELNYX_KEY'));
                        $info = \Telnyx\Message::Create([
                             'messaging_profile_id' => env('MESSAGE_PROFILE_ID'),
                            //'from' => '+18162083113',
                            'to'   => $user->mobile_number,
                            /*'text' => "Hello $user->name.This is a reminder text to meet Faith Properties at $user->what_address_you_are_coming_to at $user->timeslot for $skill_names.Please call 8168067811 if you are not able to make meeting.",*/
                            'text' => "Hello $user->name.This is a reminder text to meet Faith Properties at $user->what_address_you_are_coming_to at $time_formet for $skill_names.Please call if you are not able to make meeting.",
                        ]);
                        if($info){
                            \App\Scheduler::where('id', $user->id)->update(['reminder_sent_on' => $date]);
                        }
                    }
                }
            } catch (\Exception $e){
                 echo $e->getMessage();
            }
        }
        //
    }
}
