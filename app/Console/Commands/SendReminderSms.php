<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use \Carbon\Carbon;
use Telnyx;

class SendReminderSms extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:send_reminder_sms';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the reminder message before 30 minutes';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sendReminderSms = \App\Bookslot::select('*')->where('schedule_message_sent', 'no')->get();
        if(!empty($sendReminderSms)){
            try{
                foreach($sendReminderSms as $user){
                    $timeslot = $user->timeslot;
                    $time = $user->schedule_date;
                    $proper_time = explode("-", $timeslot);
                    $combine_datetime = "$time $proper_time[0]";
                    $date = date("Y-m-d H:i");
                    $time = strtotime($combine_datetime);
                    $time = $time - (30 * 60);
                    $combine_datetime = date("Y-m-d H:i:s", $time);
                    $current_datetime = Carbon::now()->format('Y-m-d H:i:s');
                    if($current_datetime >= $combine_datetime){
                        \Telnyx\Telnyx::setApiKey(env('TELNYX_KEY'));
                        $info = \Telnyx\Message::Create([
                            'messaging_profile_id' => env('MESSAGE_PROFILE_ID'),
                            // 'from' => '+18162083113',
                            'to'   => $user->sent_text_message_to,
                            'text' => (!empty($user->schedule_message)) ? ("Hello $user->name,this is a reminder to meet Faith Properties at $user->timeslot, property location $user->what_address_you_are_coming_to ". $user->schedule_message) : ("Hello $user->name, this is a reminder to meet Faith Properties at $user->timeslot, property location $user->what_address_you_are_coming_to"),
                        ]);
                        if($info){
                            \App\Bookslot::where('id', $user->id)->update(['schedule_message_sent' => 'yes']);
                        }
                    }
                }
            } catch (\Exception $e){
                 echo $e->getMessage();
            }
        }
    }
}
