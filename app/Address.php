<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'address', 'city', 'state', 'first_name', 'last_name', 'phone_no', 'email', 'linkedin_handler', 'twitter_handler', 'facebook_handler', 'sms', 'email', 'added_on', 'email_sent_on', 'sms_sent_on',
    ];
}
