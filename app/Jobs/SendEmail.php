<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendEmail implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id = $this->id;
        $data = [];
        try {
            $address = \App\Address::where('id', $id)
                ->whereNotNull('email')
                ->where('mail', 'Not Yet')
                ->first();

            if (!empty($address)) {
                $setting = \App\Setting::select('email_content')->where('id', 1)->first();
                $email_content = $setting->email_content;

                $mail = \Mail::send([], [], function ($message) use ($address, $email_content) {
                    $message->from('projects@3rdwave-marketing.com', 'Faith Properties Lead Generation')
                        ->to($address->email, $address->first_name)
                        ->subject("Address added to database")
                        ->setBody($email_content, 'text/html');
                });
                $data['mail'] = "Sent";
                $data['email_sent_on'] = now();

                if (!empty($data)) {
                    \App\Address::where('id', $id)->update($data);
                }
            }
        } catch (\Exception $e) {
            $data['mail'] = "Failed";
            $data['email_sent_on'] = now();

            if (!empty($data)) {
                \App\Address::where('id', $id)->update($data);
            }
            \Log::error('Email Job Failed due to - ' . $e->getMessage());
        }
    }
}
