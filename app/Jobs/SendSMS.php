<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendSMS implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $id = $this->id;
        $data = [];
        try {
            $id = $this->id;

            $address = \App\Address::where('id', $id)
                ->whereNotNull('phone_no')
                ->where('sms', 'Not Yet')
                ->first();

            if (!empty($address)) {
                $setting = \App\Setting::select('sms_content')->where('id', 1)->first();
                $sms_content = $setting->sms_content;

                $info = \Nexmo::message()->send([
                    'to' => $address->phone_no,
                    'from' => "18162276099",
                    'text' => strip_tags($sms_content),
                ]);
                $data['sms'] = "Sent";
                $data['sms_sent_on'] = now();

                if (!empty($data)) {
                    \App\Address::where('id', $id)->update($data);
                }
            }
        } catch (\Exception $e) {
            $data['sms'] = "Failed";
            $data['sms_sent_on'] = now();

            if (!empty($data)) {
                \App\Address::where('id', $id)->update($data);
            }
            \Log::error('SMS Job Failed due to - ' . $e->getMessage());
        }
    }
}
