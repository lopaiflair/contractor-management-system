<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email_content', 'sms_content', 'ph_numbers_to_send_notification', 'report_notification', 'send_email_after_days', 'send_email_after_hours', 'send_email_after_minutes', 'report_notification_content', 'send_sms_after_days', 'send_sms_after_hours', 'send_sms_after_minutes',
    ];
}
