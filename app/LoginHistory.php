<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LoginHistory extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'action',
    ];

    protected $table = 'login_history';

    public function admin()
    {
        return $this->belongsTo('App\Admin', 'user_id');
    }
}
