<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Bookslot;
use App\Skill;
use DB;
/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class BookslotController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->time_slot_array = array(
            "10:00-10:30" => "5837 Tracy KCMO 64110",
            "10:45-11:15" => "3406 Michigan Ave KCMO 64109",
            "11:45-12:15" => "4218 Virginia KCMO 64110",
            "12:30-01:00" => "2405 Prospect KCMO 64127",
            "01:15-01:45" => "2637 Garfield KCMO 64127",
            "02:00-02:30" => "4216 Virginia KCMO 64110",
            "02:45-03:15" => "703 East 43rd Street KCMO 64110"
        );
        $this->time_conversation = array(
            "10:00-10:30" => "10:00am - 10:30am",
            "10:45-11:15" => "10:45am - 11:15am",
            "11:45-12:15" => "11:45am - 12:15pm",
            "12:30-01:00" => "12:30pm - 01:00pm",
            "01:15-01:45" => "01:15pm - 01:45pm",
            "02:00-02:30" => "02:00pm - 02:30pm",
            "02:45-03:15" => "02:45pm - 03:15pm"
        );
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
   /* public function index()
    {
        return view('admin.dashboard')->with('contentheader_title', "Dashboard");
    }*/
    public function index(){
          return view('schedule_contractors.index')->with('contentheader_title', "Schedule Contractors");
    }
    public function save_date(Request $request){

        $timestamp = strtotime($request->schedule_date);
        $fullday=date('l', $timestamp);
        $month=date('F', $timestamp);
        $year=date('Y', $timestamp);
        $date=date('d', $timestamp);

        $time_conversation_array=$this->time_conversation;
        $start_time=$request->start_time;
        $end_time=$request->end_time;
        $timeslot=$start_time."-".$end_time;
        $start_time_12=date('h:ia', strtotime($start_time));
        $end_time_12=date('h:ia', strtotime($end_time));
        $start = strtotime($start_time);
        $end = strtotime($end_time);
        $mins = ($end - $start) / 60;
         
  
        $date_string=$start_time_12." - ".$end_time_12.", ".$fullday.", ".$month." ".$date.", ".$year;
        $request->session()->put('schedule_contractor_schedule_date', $request->schedule_date);
        $request->session()->put('schedule_contractor_timeslot',$timeslot);
        $request->session()->put('schedule_contractor_fulldate',$date_string); 
        $request->session()->put('schedule_contractor_total_mins',$mins); 
        //$get_add_val=$this->get_address($request->timeslot);
        //$request->session()->put('schedule_contractor_add_val',$get_add_val);
        $skills = Skill::where('status',1)->orderBy('skill','asc')->get()->toarray();
        //  echo "<pre>";print_r($skills);die();
         return view('schedule_contractors.create')->with('contentheader_title', "")->with('skills',$skills)->with('date_string',$date_string);
          
    }
     public function checkemail(Request $request){

        $email  = $request->email;
       
        $email_count = Bookslot::where('email', '=', $email)
                             ->count();
            if($email_count > 0){
                return 'false';
            }else{
                return 'true';
            }
       
        
    }
    public function store(Request $request){

        $this->validate($request, [
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required'],
          'what_address_you_are_coming_to' => ['required'],
          'mobile_number' => ['required'],
          'skill_set' => ['required'],
          'sent_text_message_to' => ['required'],
        ]);
        if(!empty($request->skill_set)){ 
            $skill_set     = implode(',', $request->skill_set);
        }else{
            $skill_set     = '';
            
        }
        $schedule_date=session('schedule_contractor_schedule_date', 'default');
        $timeslot=session('schedule_contractor_timeslot', 'default');
        $orgDate = $schedule_date;  
        $newDate = date("Y-m-d", strtotime($orgDate));  
          $category = Bookslot::create([
            'schedule_date' =>$newDate,
            'timeslot' => $timeslot,
            'name' => $request->name,
            'email' => $request->email,
            'what_address_you_are_coming_to' => $request->what_address_you_are_coming_to,
            'mobile_number' => $request->mobile_number,
            'skill_set' => $skill_set,
            'sent_text_message_to' => $request->sent_text_message_to,
            'notes' => $request->notes,
         ]);

         return redirect()->route('schedule_contractors_list')
            ->with('success', 'Schedule Contractor created successfully');
    }
    function get_address($time_slot){
        $time_slot_array = $this->time_slot_array;
    
        return $time_slot_array[$time_slot];

    }
    function list(){
         return view('schedule_contractors.list')->with('contentheader_title', "Schedule Contractor List");
    }
    public function get_datatable_data(Request $request)
    {
        $columns = array( 
                            0 =>'id', 
                            1 => "id",
                            2 =>'schedule_date',
                            3=> 'timeslot',
                            4=> 'name',
                            5=> 'email',
                            6=> 'what_address_you_are_coming_to',
                            7=> 'mobile_number',
                            8=> 'skill_set',
                            9=> 'sent_text_message_to',
                            10=> 'notes',
                            11=> 'reminder'
                        );
        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');
 
         $sql = \DB::table("contractor_schedules")
                        ->select("contractor_schedules.*",\DB::raw("GROUP_CONCAT(skills.skill) as skillname"))
                        ->leftjoin("skills",\DB::raw("FIND_IN_SET(skills.id,contractor_schedules.skill_set)"),">",\DB::raw("'0'"))
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->groupBy("contractor_schedules.id");  

        if (isset($request->search['value'])) {

            $skillids  = Skill::select('id')->where('skill', $request->search['value'] )->first();
             if(!empty($skillids)){
                $searchid = $skillids->id;
                  $sql->where(function ($query) use ($request,$searchid) {
                    $query->Where('contractor_schedules.schedule_date', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.timeslot', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.name', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.email', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.what_address_you_are_coming_to', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.mobile_number', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.skill_set', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.sent_text_message_to', 'like', '%' . $request->search['value'] . '%')
                        ->orwhereRaw("FIND_IN_SET({$searchid},contractor_schedules.skill_set)")
                        ->orWhere('contractor_schedules.notes', 'like', '%' . $request->search['value'] . '%');
                });

            }else{
           
                $sql->where(function ($query) use ($request) {
                    $query->Where('contractor_schedules.schedule_date', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.timeslot', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.name', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.email', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.what_address_you_are_coming_to', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.mobile_number', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.skill_set', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.sent_text_message_to', 'like', '%' . $request->search['value'] . '%')
                        ->orWhere('contractor_schedules.notes', 'like', '%' . $request->search['value'] . '%');
                });
            }
             
            
        }
        

        $recordsTotal = $sql->get()->count();
        $data = $sql->limit($request->length)->skip($request->start)->get();
        $skillArr = [];
        $json['data'] = $data;
        $json['draw'] = $request->draw;
        $json['recordsTotal'] = $recordsTotal;
        $json['recordsFiltered'] = $recordsTotal;

        return json_encode($json);
    }
    function changestatus(Request $request){
        DB::table('contractor_schedules')->where('id', $_POST['id'])->update(array('reminder' => $_POST['reminder']));  
        $request->session()->flash('success', 'Status changed successfully.');


    }
    function store_model_value(){
        DB::table('contractor_schedules')->whereIn('id', $_POST['chkbox'])->update(array('reminder_message' => $_POST['reminder_message'],'reminder_interval' => $_POST['interval'],'reminder' => $_POST['reminder_notification'],'hired' => "yes"));
        $request->session()->flash('success', 'Data saves successfullay.');

    }
    function edit($id){
         $data=  Bookslot::find($id);
         $skills = Skill::where('status',1)->orderBy('skill', 'ace')->get()->toarray();
         return view('schedule_contractors.edit', compact('data'))->with('contentheader_title', "")->with('skills',$skills);
 
    }
    function edit_store(Request $request){
         DB::table('contractor_schedules')->where('id', $request->id)->update(array('notes' => $request->notes,'schedule_message' => $request->schedule_message));
         return redirect()->route('schedule_contractors_list')
            ->with('success', 'Schedule Contractor edited successfully');
    }
}
