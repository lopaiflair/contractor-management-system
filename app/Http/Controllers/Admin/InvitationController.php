<?php

namespace App\Http\Controllers\Admin;

use App\Contractors;
use App\Http\Controllers\Controller;
use App\Skill;
use App\template;
use DB;
use Excel;
use Illuminate\Http\Request;
use Log;
use Mail;
use Telnyx;
use URL;

class InvitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Skills = Skill::orderBy('skill', 'ASC')->where('status', 1)->get()->toarray();
        $template = template::orderBy('id', 'desc')->where('status', "active")->get()->toarray();
        $Contractors = Contractors::orderBy('name', 'ASC')->where('name', '!=', '')->get(['id','name'])->toarray();
        $mobile_number = DB::table('mobile_data')->get()->toArray();
        $getActivenumber = DB::table('mobile_data')->where('status', 1)->first();
        if (!empty($getActivenumber)) {
            $active_number = $getActivenumber->mobileno;
        } else {
            $active_number = '';
        }

        return view('admin.invite.invitation')->with('contentheader_title', "Invite Contractor To Bid")->with('Contractors', $Contractors)->with('Skills', $Skills)->with('mobile_number', $mobile_number)->with('active_number', $active_number)->with('template', $template);
    }
    public function getCountContactor(Request $request)
    {
        $count = 0;
        $valueArr = [];
        foreach ($request->skills as $skey => $svalue) {
            $getcon = Contractors::select('id')
                ->whereRaw("find_in_set('" . $svalue . "',skills)")
                ->get()
                ->toArray();

            if (!empty($getcon)) {
                foreach ($getcon as $getconkey => $getconvalue) {
                    $valueArr[] = $getconvalue['id'];

                }
            }

        }
        $contractors = !empty($valueArr) ? array_unique($valueArr) : [];
        $count = !empty($contractors) ? count($contractors) : 0;
        $arr = array('count' => $count);
        echo json_encode($arr);exit;
    }
    public function sendmessage(Request $request)
    {
        if (isset($request->skills)) {
            $skills = $request->skills;
            $getContractors = new Contractors;
            $getContractors = $getContractors->where(function($q) use ($skills) {
                foreach ($skills as $skey => $svalue) {
                    $q->orWhereRaw("find_in_set('{$svalue}',skills)");
                }
            });
            $contractors = $getContractors->groupBy('id')->orderBy('id')->pluck('id')->toArray();
        } else if(!empty($request->contractors)) {
            $contractors = Contractors::whereIn('name', array_filter(array_map('trim',explode(",",$request->contractors))))->pluck('id')->toArray();
        }
        $destinationImage = '';
        $messageImage = '';
        $extension = '';
        $addInvite = [];
        $shorturl = "";
        $addInvite['document'] = null;
        if ($request->hasFile('document')) {
            $image = $request->file('document');
            $name = time() . '.' . $image->getClientOriginalExtension();
            $extension = pathinfo($name, PATHINFO_EXTENSION);
            \Storage::disk('local')->putFileAs('public/invite_uploads', $image, $name);
            $destinationImage = public_path('/storage/invite_uploads/' . $name);
            $messageImage = url('/public/storage/invite_uploads/'.$name);
            $addInvite['document'] = $name;
            $shorturl = $this->generateShortUrl($messageImage, $request->project_name);
        }
        $addInvite['project_name'] = $request->project_name;
        $addInvite['contractor_type'] = $request->typeof;
        $addInvite['skills'] = !empty($request->skills) ? serialize($request->skills) : '';
        $addInvite['message'] = $request->message;
        $addInvite['total_count'] = count($contractors);
        $addInvite['sent_by'] = $request->mobile_number;
        $addInvite['created_at'] = date('Y-m-d H:i:s');
        $invited_at = DB::table('invite')->insertGetId($addInvite);

        if (!empty($contractors)) {
            $contractor_record = Contractors::whereIn('id', $contractors)->get();
            $records = array();
            foreach ($contractor_record as $key => $value) {
                \Log::debug(print_r($value, true));
                $invitation_send = array();
                $mobile_no = $value->send_mobileno;
                $email = $value->email;
                $name = $value->name;
                $textmessage = $request->message;
                $invitation_send['invite_id'] = $invited_at;
                $invitation_send['contractor_id'] = $value->id;
                $invitation_send['mobile_no'] = $value->send_mobileno;
                $invitation_send['email'] = $value->email;
                $invitation_send['created_at'] = date('Y-m-d H:i:s');
                $invitation_send['message_sent'] = 0;
                if (!empty($mobile_no)) {
                    try {
                        $message_profile_id = "40017350-90c0-4542-8439-41189f262012";
                        \Telnyx\Telnyx::setApiKey('KEY01734F82F8A888910BF268CD1D21D00B_NM5699P4GKPD8lAeqAXdJX');
                        $info = \Telnyx\Message::Create([
                            //'from' => '+18162083113',
                            'messaging_profile_id' => $message_profile_id,
                            'to' => "+" . $mobile_no,
                            'text' => strip_tags($textmessage) . ' ' . $shorturl,
                            'webhook_url' => url('success_webook.php'),
                            'webhook_failover_url' => url('fail_over_webhook.php'),
                        ]);
                        $invitation_send['message_sent'] = 1;
                    } catch (\Exception $e){
                        \Log::debug("Fail to send sms due to - ".$e->getMessage());
                    }
                }
                $invitation_send['email_sent'] = 0;
                if (!empty($email)) {
                    try {
                        Mail::send([], [], function ($message) use ($email, $name, $destinationImage, $textmessage, $shorturl, $extension, $request) {
                            $message->to($email, $name)->subject("Invitation for Contractor system");
                            if ($request->hasFile('document')) {
                                if ($extension == 'mp4') {
                                    $message->setBody(strip_tags($textmessage) . '<br/><br/> Please find video link below:- ' . '<br/>' . $shorturl, 'text/html');
                                } else {
                                    $message->attach($destinationImage)
                                        ->setBody(strip_tags($textmessage) . "<br/><br/>" . $shorturl, 'text/html');
                                }
                            } else {
                                $message->setBody(strip_tags($textmessage), 'text/html');
                            }
                        });
                        $invitation_send['email_sent'] = 1;
                    } catch (\Exception $e){
                        \Log::debug("Fail to send email due to - ".$e->getMessage());
                    }
                }
                $records[] = $invitation_send;
            }
            DB::table('invite_send')->insert($records);
        }
        return response()->json([
            'status' => true,
            "message" => "Invitation send successfully.",
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function report_invitation()
    {
        return view('admin.invite.invitation_report')->with('contentheader_title', "Invitation Report");
    }
    public function get_datatable_data(Request $request)
    {
        $columns = array(0 => 'project_name',
            1 => 'contractor_type',
            2 => 'sent_by',
            3 => 'total_count',
            4 => 'created_at',
        );
        if ($request->input('order')) {
            $order = $columns[$request->input('order.0.column')];
            $dir = $request->input('order.0.dir');
        } else {
            $order = 'id';
            $dir = 'desc';
        }
        $sql = DB::table('invite')->select('id', 'project_name', 'contractor_type', 'total_count', 'sent_by', 'created_at')->orderBy($order, $dir);

        if (isset($request->search['value'])) {
            $sql->where(function ($query) use ($request) {
                $query->Where('project_name', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('contractor_type', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('total_count', 'like', '%' . $request->search['value'] . '%')
                    ->orWhere('sent_by', 'like', '%' . $request->search['value'] . '%');

            });
        }

        $recordsTotal = $sql->count();
        $data = $sql->limit($request->length)->skip($request->start)->get()->toArray();
        $addData = [];
        if (!empty($data)) {
            foreach ($data as $dataval) {
                $editurl = "view/" . $dataval->id;
                $exporturl = "export/" . $dataval->id;
                $nestedData['id'] = $dataval->id;
                $nestedData['project_name'] = $dataval->project_name;
                $nestedData['contractor_type'] = $dataval->contractor_type;
                $nestedData['sent_by'] = $dataval->sent_by;
                $nestedData['total_count'] = $dataval->total_count;
                $nestedData['created_at'] = $dataval->created_at;
                $nestedData['action'] = '<a class="editbtn" data-placement="top" data-toggle="tooltip" data-original-title="View" href="' . $editurl . '">View</a><a class="editbtn" data-placement="top" data-toggle="tooltip" data-original-title="Export" href="' . $exporturl . '" style="margin-left:4px;">Export</a>';
                $addData[] = $nestedData;

            }
        }
        $json['data'] = $addData;
        $json['draw'] = $request->draw;
        $json['recordsTotal'] = $recordsTotal;
        $json['recordsFiltered'] = $recordsTotal;

        return json_encode($json);

    }
    public function show_details($invite_id)
    {
        $getinvite = DB::table('invite')->where('id', $invite_id)->first();
        $getinvite_details = DB::table('invite_send')->where('invite_id', $invite_id)->get()->toArray();
        return view('admin.invite.report_detail')->with('contentheader_title', "Invitation Report Detail")->with('getinvite', $getinvite)->with('getinvite_details', $getinvite_details);

    }
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function GetAllContactor()
    {
        $Contractors = Contractors::orderBy('name', 'ASC')->where('name', '!=', '')->get()->toarray();
        $response = array();
        foreach ($Contractors as $key => $employee) {
            $response[] = array(
                "id" => $employee['id'],
                "text" => $employee['name'],
            );
        }
        echo json_encode($response);
        exit;
    }

    public function ExportProjectData($invite_id)
    {
        $data = DB::table('contractors')
            ->select('contractors.*', 'invite_send.*', DB::raw("(GROUP_CONCAT(skills.skill)) as 'con_skills'"))
            ->join('invite_send', 'invite_send.contractor_id', '=', 'contractors.id')
            ->leftJoin('skills', function ($join) {
                $join->whereRaw('FIND_IN_SET(skills.id, contractors.skills)');
            })
            ->where('invite_id', $invite_id)
            ->groupBy('contractors.id')
            ->orderBy('contractors.id', 'ASC')
            ->get();

        //echo "<pre>";print_r($data);exit;
        $column_array[] = array('Contractor Name', 'Email', 'Mobile Number', 'Skills', 'Comment');
        foreach ($data as $main_data) {
            $column_array[] = array(
                'Contractor Name' => $main_data->name,
                'Email' => $main_data->email,
                'Mobile Number' => $main_data->mobile_no,
                'Skills' => $main_data->con_skills,
                'Comment' => $main_data->comment,
            );
        }
        Excel::create('Customer Data', function ($excel) use ($column_array) {
            $excel->setTitle('Customer Data');
            $excel->sheet('Customer Data', function ($sheet) use ($column_array) {
                $sheet->fromArray($column_array, null, 'A1', false, false);
            });
        })->download('xlsx');
    }

    public function generateShortUrl($messageImage, $project_name){
        $url = env('THREEWM_REACH_URL');
        $post = [
            'username' => env('THREEWM_REACH_USERNAME'),
            'password' => env('THREEWM_REACH_PASSWORD'),
            'action' => 'shorturl',
            'url' => $messageImage,
            'title' => $project_name,
            'format' => 'json',
        ];
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));
        $response = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($response);
        if(!empty($response) && isset($response->shorturl)){
            return  $response->shorturl;
        } else {
            return  "";
        }
    }

    public function getContractors(Request $request){
        if(!empty($request->term)){
            $Contractors = Contractors::orderBy('name', 'ASC')
                ->where('name','like',"{$request->term}%");
            if(!empty(array_filter(explode(",",$request->selected)))){
                $Contractors = $Contractors->whereNotIn('name',array_filter(explode(", ",$request->selected)));
            }
            $Contractors = $Contractors->get(['id','name']);
            $data = [];
            if(!empty($Contractors)){
                foreach ($Contractors as $key => $value) {
                    $tmp = [];
                    $tmp['value'] = $value->id;
                    $tmp['label'] = $value->name;
                    $data[] = $tmp;
                }
            }
            return response()->json($data);
        } else {
            return;
        }
    }
}
