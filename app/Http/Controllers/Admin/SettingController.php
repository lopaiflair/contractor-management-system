<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Contractors;
use App\Skill;
use App\Mobile_data;
use App\Jobs\SendEmail;
use App\Jobs\SendSMS;
use Excel;
use Mail;
use DB;
use Log;
use Nexmo;
use URL;
use Illuminate\Routing\UrlGenerator;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.settings.settings')->with('contentheader_title', "Settings");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($setting_id = '')
    {
        if(!empty($setting_id)){
            $seting_data = DB::table('mobile_data')->where('id',$setting_id)->first();
        }else{
            $seting_data = [];
        }
        $active_count = DB::table('mobile_data')->where('status',1)->count();

        return view('admin.settings.createsetting')->with('contentheader_title', "Settings")->with('seting_data',$seting_data)->with('active_count',$active_count);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
        $data['mobileno']   = !empty($request->mobileno) ? $request->mobileno : '';
        
        if(!empty($request->setting_id)){
            if(isset($request->status)){
                $data['status'] = $request->status;
                
            }else{
                $data['status'] = 0;
            }
            
            DB::table('mobile_data')->where('id',$request->setting_id)->update($data);
            if(isset($request->status)){
                $updateData['status'] = 0;
                DB::table('mobile_data')->where('id','!=',$request->setting_id)->update($updateData);
            }
            $success = 'Record updated successfully';
        }else{
            $data['status'] = isset($request->status) ? $request->status : 0;
            $data['created_at'] = date('Y-m-d H:i:s');
            $addId   = DB::table('mobile_data')->insertGetId($data);

            if(isset($request->status)){
                $updateData['status'] = 0;
                DB::table('mobile_data')->where('id','!=',$addId)->update($updateData);
            }
          
            $success = 'Record added successfully';
        }
        
        
        return redirect('setting')->with('success', $success);
    }

    public function get_datatable_data(Request $request){
       /* $columns = array(   0 =>'role',
                             1=> 'role_long',
                    );*/
       
        // $order = $columns[$request->input('order.0.column')];
        // $dir = $request->input('order.0.dir');
        // $sql = Profile::select('id', 'role','role_long','description','created_at')->orderBy($order,$dir);
        $sql = DB::table('mobile_data')->select('id', 'mobileno','status','created_at')->orderBy('id','desc');
        
        if (isset($request->search['value'])) {
            $sql->where(function ($query) use ($request) {
                $query->Where('mobileno', 'like', '%' . $request->search['value'] . '%');
                   
            });
        }

        $recordsTotal = $sql->count();
        $data = $sql->limit($request->length)->skip($request->start)->get()->toArray();
        $addData = [];
         if(!empty($data)){
            foreach($data as $dataval){
                $deleteurl  = "setting/delete/".$dataval->id;
                $editurl    = "setting/".$dataval->id.'/create';
                $onclickmsg = "return confirm('Are you sure you want to delete this record?');";
                $nestedData['id']               = $dataval->id;
                $nestedData['mobileno']         = $dataval->mobileno;
                if($dataval->status == 1){
                    $nestedData['status']           ='<span class="label label-success">Yes</span>';
                }else{
                    $nestedData['status']           = '<span class="label label-danger">No</span>';
                }
                // $nestedData['created_at']       = $dataval->created_at;
                $nestedData['created_at']       =  date('d F, Y', strtotime($dataval->created_at));

                $nestedData['action']           = ' <a class="editbtn" data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="'.$editurl.'">Edit</a> <a href="'.$deleteurl.'" onclick="'.$onclickmsg.'" class="deletebtn" data-toggle="tooltip" data-original-title="Delete" aria-describedby="tooltip234960">Delete</a>';
                $addData[] = $nestedData;

            }
        }
        $json['data'] = $addData;
        $json['draw'] = $request->draw;
        $json['recordsTotal'] = $recordsTotal;
        $json['recordsFiltered'] = $recordsTotal;

        return json_encode($json);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('mobile_data')->where('id',$id)->delete();
        return redirect('setting')->with('error', 'Record removed successfully');
    }
}
