<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Skill;
use App\Contractors;

class SkillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   //echo 'hxcxzc';exit;
         return view('admin.skills.skills')->with('contentheader_title', "Skills");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.skills.createskill')->with('contentheader_title', "Skills");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // pre($request->all());exit();
        $data['skill']       = $request->skill;
        $data['status']      = $request->status;
       
        if(isset($request->skill_id)){
            $addId = Skill::where('id',$request->skill_id)->update($data);
            $success = 'Record updated successfully';
        }else{
            $data['created_at'] = date('Y-m-d H:i:s');
            $addId = Skill::insert($data);
            $success = 'Record added successfully';
        }
        
        return redirect('skills')->with('success', $success);
        
    }
      public function get_datatable_data(Request $request)
    {
        $sql = Skill::select('id', 'skill','status','created_at')->orderBy($request->columns[$request->order[0]['column']]['name'], $request->order[0]['dir']);

        if (isset($request->search['value'])) {
            $sql->where(function ($query) use ($request) {
                $query->Where('skill', 'like', '%' . $request->search['value'] . '%');
                  
            });
        }

        $recordsTotal = $sql->count();
        $data = $sql->limit($request->length)->skip($request->start)->get();

        $json['data'] = $data;
        $json['draw'] = $request->draw;
        $json['recordsTotal'] = $recordsTotal;
        $json['recordsFiltered'] = $recordsTotal;

        return json_encode($json);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $editeData = Skill::where('id',$id)->first();
        return view('admin.skills.createskill')->with('contentheader_title', "Skills")->with('editeData',$editeData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Skill::where('id',$id)->delete();
        return redirect('skills')->with('error', 'Record removed successfully');
    }
}
