<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\template;
use DB;

class TemplateController extends Controller
{
    public function index()
    { 
        
        return view('admin.template.template')->with('contentheader_title', "Template Management");
    }

    public function GetAllTemplateData(Request $request)
    {
    	$sql = template::select('id', 'name','description','status')->orderBy($request->columns[$request->order[0]['column']]['name'], $request->order[0]['dir']);
    
    	if (isset($request->search['value'])) {
            $sql->where(function ($query) use ($request) {
                $query->Where('name', 'like', '%' . $request->search['value'] . '%')
                ->orWhere('description', 'like', '%' . $request->search['value'] . '%')
                ->orWhere('status', 'like', '%' . $request->search['value'] . '%');
            });
            
        }

        $recordsTotal = $sql->get()->count();
        $data = $sql->limit($request->length)->skip($request->start)->get();
        $skillArr = [];
        $json['data'] = $data;
        $json['draw'] = $request->draw;
        $json['recordsTotal'] = $recordsTotal;
        $json['recordsFiltered'] = $recordsTotal;

        return json_encode($json);
    }

    public function CreateTemplate()
    {
       return view('admin.template.createtemplate')->with('contentheader_title', "Template");
    }

    public function StoreTemplate(Request $request)
    {
        $data['name']        = $request->name;
        $data['description'] = $request->description;
        $data['status']      = $request->status;
       
        if(isset($request->template_id)){
            $addId = template::where('id',$request->template_id)->update($data);
            $success = 'Record updated successfully';
        }else{
            $data['created_at'] = date('Y-m-d H:i:s');
            $addId = template::insert($data);
            $success = 'Record added successfully';
        }
        
        return redirect('template')->with('success', $success);
        
    }

    public function EditTemplate($id)
    {
        $editeData = template::where('id',$id)->first();
        return view('admin.template.createtemplate')->with('contentheader_title', "Template")->with('editeData',$editeData);
    }

    public function DelteTemplate($id)
    {
        template::where('id',$id)->delete();
        return redirect('template')->with('error', 'Record removed successfully');
    }
}
