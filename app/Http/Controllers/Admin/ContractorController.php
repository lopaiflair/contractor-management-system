<?php

namespace App\Http\Controllers\Admin;
use App\Contractors;
use App\Http\Controllers\Controller;
use App\Skill;
use DB;
use Excel;
use Illuminate\Http\Request;

class ContractorController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index() {

		return view('admin.contractors.contractors')->with('contentheader_title', "Contractor Database");
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create() {
		$skills = Skill::where('status', 1)->get()->toarray();
		return view('admin.contractors.createcontractors')->with('contentheader_title', "Contractor Database")->with('skills', $skills);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request) {
		// pre($request->all());exit;
		$data['name'] = !empty($request->name) ? $request->name : '';
		$data['email'] = !empty($request->email) ? $request->email : '';
		if (!empty($request->skill)) {
			$data['skills'] = implode(',', $request->skill);
		} else {
			$data['skills'] = '';

		}

		$data['mobile_no'] = !empty($request->mobileno) ? $request->mobileno : '';
		$data['send_mobileno'] = !empty($request->mobileno) ? '91' . $request->mobileno : '';
		$data['office'] = !empty($request->office) ? $request->office : '';
		$data['comment'] = !empty($request->comment) ? $request->comment : '';
		$data['skill_contact'] = !empty($request->skill_contact) ? $request->skill_contact : '';
		/* $skillArr = [];
			        $nameArr  = [];

			        if(isset($request->skill)){
			            foreach ($request->skill as $key => $value) {

			                    $skillval = Skill::select('skill')->where('id',$value)->first();
			                    pre($skillval->skill);
			                    // $skillArr[] = array_push($skillval->skill);

			             }
			        }
			         pre($skillArr);exit;
		*/
		if (!empty($request->name) || !empty($request->email) || !empty($request->skill_contact) || !empty($request->office) || !empty($request->mobileno) || !empty($request->comment) || isset($request->skill)) {

			if (isset($request->contractor_id)) {
				$addId = Contractors::where('id', $request->contractor_id)->update($data);
				$success = 'Record updated successfully';
			} else {
				$data['created_at'] = date('Y-m-d H:i:s');
				$addId = Contractors::insert($data);
				$success = 'Record added successfully';
			}
		} else {
			$success = 'No record inserted';
		}
		return redirect('setting')->with('success', $success);

	}

	public function get_datatable_data(Request $request) {
		$sql = \DB::table("contractors")
			->select("contractors.*", \DB::raw("GROUP_CONCAT(skills.skill) as skillname"))
			->leftjoin("skills", \DB::raw("FIND_IN_SET(skills.id,contractors.skills)"), ">", \DB::raw("'0'"))
			->groupBy("contractors.id");
		if (isset($request->search['value'])) {
			$skillids = Skill::select('id')->where('skill', $request->search['value'])->first();

			if (!empty($skillids)) {
				$searchid = $skillids->id;
				$sql->where(function ($query) use ($request, $searchid) {
					$query->Where('contractors.name', 'like', '%' . $request->search['value'] . '%')
						->orWhere('contractors.email', 'like', '%' . $request->search['value'] . '%')
						->orWhere('contractors.mobile_no', 'like', '%' . $request->search['value'] . '%')
						->orwhereRaw("FIND_IN_SET({$searchid},contractors.skills)")
						->orWhere('contractors.office', 'like', '%' . $request->search['value'] . '%');
				});
			} else {
				$sql->where(function ($query) use ($request) {
					$query->Where('contractors.name', 'like', '%' . $request->search['value'] . '%')
						->orWhere('contractors.email', 'like', '%' . $request->search['value'] . '%')
						->orWhere('contractors.mobile_no', 'like', '%' . $request->search['value'] . '%')
						->orWhere('contractors.office', 'like', '%' . $request->search['value'] . '%');
				});
			}

		}

		$recordsTotal = $sql->get()->count();
		$data = $sql->limit($request->length)->skip($request->start)->get();
		$skillArr = [];
		$json['data'] = $data;
		$json['draw'] = $request->draw;
		$json['recordsTotal'] = $recordsTotal;
		$json['recordsFiltered'] = $recordsTotal;

		return json_encode($json);
	}

	/*public function importdata(Request $request){
		         try {
		            if ($request->hasFile('import_csv')) {
		                $data = Excel::load(
		                    $request->file('import_csv')->getRealPath(),
		                    function ($reader) {
		                        $reader->ignoreEmpty();
		                    }
		                )->get()->toArray();

		                foreach (array_filter($data) as $fkey => $value) {
		                    $addArr = [];
		                    if(isset($value['skill'])){
		                        $myArr = explode(',',$value['skill'] );

		                         if(!empty($myArr)){
		                            foreach ($myArr as $key => $val) {
		                                $skillID = Skill::select('id')->where('status',1)
		                                            ->where('skill',$val)->first();
		                                if(!empty($skillID)){
		                                    $addArr[$key] = $skillID->id;
		                                }

		                            }
		                        }

		                    }
		                    $mynewArr = implode(',', $addArr);
		                    $row['skills']  = !empty($mynewArr) ? $mynewArr : '';
		                    $row['name'] = isset($value['contractor']) ? $value['contractor'] : '';
		                    $row['email']      = isset($value['email']) ? $value['email'] : '';
		                    //$row['skills']     = isset($value['contractor']) ? $value['contractor'] : '';
		                    $row['skill_contact'] = isset($value['skillcontact']) ? $value['skillcontact'] : '';
		                    $row['mobile_no'] = isset($value['mobile']) ? $value['mobile'] : '';
		                    $row['send_mobileno'] = isset($value['mobile']) ? '91'.$value['mobile'] : '';
		                    $row['office'] = isset($value['office']) ? $value['office'] : '';
		                    $row['comment'] = isset($value['comments']) ? $value['comments'] : '';
		                    $row['created_at'] = date('Y-m-d H:i:s');

		                    // $raw = array();
		                    // isset($value['address']) ? $raw['address'] = $value['address'] : $raw['address'] = "";
		                    // isset($value['city']) ? $raw['city'] = $value['city'] : $raw['city'] = "";
		                    // isset($value['state']) ? $raw['state'] = $value['state'] : $raw['state'] = "";
		                    // $raw['added_on'] = now();

		                    // $res = $this->getInformation($raw);

		                    $insert = Contractors::insert($row);
		                    // if ($insert) {
		                    //     SendEmail::dispatch($insert->id)->delay(now()->addMinutes($this->email_send_at['minutes'])->addHours($this->email_send_at['hours'])->addDays($this->email_send_at['days']));
		                    //     SendSMS::dispatch($insert->id)->delay(now()->addMinutes($this->sms_send_at['minutes'])->addHours($this->sms_send_at['hours'])->addDays($this->sms_send_at['days']));
		                    // }
		                }

		                return redirect('contractors')
		                    ->with('success', "File imported successfully");
		            } else {
		                return back()->with('error', 'Please provide file to import');
		            }
		        } catch (\Exception $e) {
		            return back()->with('error', 'Failed due to ' . $e->getMessage());
		        }
	*/

	public function importdata(Request $request) {
		try {
			if ($request->hasFile('import_csv')) {
				$data = Excel::load(
					$request->file('import_csv')->getRealPath(),
					function ($reader) {
						$reader->ignoreEmpty();
					}
				)->get()->toArray();

				foreach (array_filter($data) as $fkey => $value) {
					$addArr = [];
					if (isset($value['skill'])) {
						$myArr = explode(',', $value['skill']);

						if (!empty($myArr)) {
							foreach ($myArr as $key => $val) {
								$skillID = Skill::select('id')->where('status', 1)
									->where('skill', $val)->first();
								if (!empty($skillID)) {
									$addArr[$key] = $skillID->id;
								} else {
									$skill_lastid = DB::table('skills')->insertGetId(
										['skill' => $val, 'status' => 1, 'created_at' => date('Y-m-d H:i:s')]
									);
									$addArr[$key] = $skill_lastid;
								}

							}
						}

					}
					$mynewArr = implode(',', $addArr);
					$row['skills'] = !empty($mynewArr) ? $mynewArr : '';
					$row['name'] = isset($value['contractor']) ? $value['contractor'] : '';
					$row['email'] = isset($value['email']) ? $value['email'] : '';
					//$row['skills']     = isset($value['contractor']) ? $value['contractor'] : '';
					$row['skill_contact'] = isset($value['skillcontact']) ? $value['skillcontact'] : '';
					$row['mobile_no'] = (!empty($value['mobile'])) ? $value['mobile'] : '';
					$row['send_mobileno'] = (!empty($value['mobile'])) ? '1' . $value['mobile'] : '';
					$row['office'] = isset($value['office']) ? $value['office'] : '';
					$row['comment'] = isset($value['comments']) ? $value['comments'] : '';
					$row['created_at'] = date('Y-m-d H:i:s');

					if (!empty($value['mobile'])) {
						$get_contractor = DB::table('contractors')->where('mobile_no', $value['mobile'])->first();
						if ($get_contractor) {
							$update = DB::table('contractors')->where('mobile_no', $value['mobile'])->update($row);
						} else {
							$insert = DB::table('contractors')->insert($row);
						}
					} else {
						$insert = DB::table('contractors')->insert($row);
					}
				}

				return redirect('contractors')
					->with('success', "File imported successfully");
			} else {
				return back()->with('error', 'Please provide file to import');
			}
		} catch (\Exception $e) {
			return back()->with('error', 'Failed due to ' . $e->getMessage());
		}
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id) {
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id) {
		$skills = Skill::where('status', 1)->get()->toarray();
		$editeData = Contractors::where('id', $id)->first();
		return view('admin.contractors.createcontractors')->with('contentheader_title', "Contractor Database")->with('editeData', $editeData)->with('skills', $skills);

	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, $id) {
		//
	}
	public function addnewskill(Request $request) {
		$data['skill'] = $request->newskill;
		$data['status'] = 1;
		$data['created_at'] = date('Y-m-d H:i:s');
		$id = DB::table('skills')->insertGetId($data);
		$Arr = array('id' => $id, 'skill' => $request->newskill);
		echo json_encode($Arr);exit;
	}
	public function addquickskill(Request $request) {
		$getSkill = DB::table('skills')->where('skill', $request->newskill)->first();
		if (empty($getSkill)) {
			$data['skill'] = $request->newskill;
			$data['status'] = 1;
			$data['created_at'] = date('Y-m-d H:i:s');
			$id = DB::table('skills')->insertGetId($data);
			$getAllskill = DB::table('skills')->orderBy('skill', 'ASC')->get()->toArray();
			$Response = array('message' => 'success', 'skillArr' => $getAllskill, 'skill' => $request->newskill);
		} else {
			$Response = array('message' => 'error');
		}
		echo json_encode($Response);exit;

	}
	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id) {
		Contractors::where('id', $id)->delete();
		return redirect('contractors')->with('error', 'Record removed successfully');
	}
	public function checkemail(Request $request) {

		$email = $request->email;

		if (!empty($request->contractor_id)) {

			$email_count = Contractors::where('email', $email)->where('id', '!=', $request->contractor_id)
				->count();

			if ($email_count > 0) {
				return 'false';
			} else {
				return 'true';
			}
		} else {

			$email_count = Contractors::where('email', '=', $email)
				->count();
			if ($email_count > 0) {
				return 'false';
			} else {
				return 'true';
			}
		}

	}
	public function checkmobile(Request $request) {
		// pre($request->all());exit;
		$mobileno = $request->mobileno;

		if (!empty($request->contractor_id)) {

			$mobile_count = Contractors::where('mobile_no', $mobileno)->where('id', '!=', $request->contractor_id)->count();

			if ($mobile_count > 0) {
				return 'false';
			} else {
				return 'true';
			}
		} else {

			$mobile_count = Contractors::where('mobile_no', '=', $mobileno)
				->count();
			if ($mobile_count > 0) {
				return 'false';
			} else {
				return 'true';
			}
		}

	}
	public function import() {
		/*$breadcrum = array();
			        $breadcrum[0]['name'] = "Contractors";
			        $breadcrum[0]['url'] = url('admin/addresses');
			        $breadcrum[1]['name'] = "Import Addresses";
		*/

		// return view('admin.contractors.import_form', compact('breadcrum'))->with('contentheader_title', 'Import Contractors');
		return view('admin.contractors.import_form')->with('contentheader_title', 'Import Contractors');
	}

	public function exportContractor(Request $request) {
		$data = DB::table('contractors')
			->select('contractors.name', 'contractors.email', 'contractors.mobile_no', 'contractors.comment', DB::raw("(GROUP_CONCAT(skills.skill)) as 'con_skills'"))
			->leftJoin('skills', function ($join) {
				$join->whereRaw('FIND_IN_SET(skills.id, contractors.skills)');
			})
			->where(function ($query) use ($request) {
				$skillids = Skill::select('id')->where('skill', $request->search)->first();
				$query->Where('contractors.name', 'like', '%' . $request->search . '%')
					->orWhere('contractors.email', 'like', '%' . $request->search . '%')
					->orWhere('contractors.mobile_no', 'like', '%' . $request->search . '%')
					->orWhere('contractors.office', 'like', '%' . $request->search . '%');
				if (!empty($skillids)) {
					$query->orwhereRaw("FIND_IN_SET({$skillids->id},contractors.skills)");
				}
			})
			->groupBy('contractors.id')
			->orderBy('contractors.id', 'ASC')
			->get();
		$column_array[] = array('Contractor Name', 'Email', 'Mobile Number', 'Skills', 'Comment');
		foreach ($data as $main_data) {
			$column_array[] = array(
				'Contractor Name' => ($main_data->name) ? $main_data->name : '',
				'Email' => ($main_data->email) ? $main_data->email : '',
				'Mobile Number' => ($main_data->mobile_no) ? $main_data->mobile_no : '',
				'Skills' => ($main_data->con_skills) ? $main_data->con_skills : '',
				'Comment' => ($main_data->comment) ? $main_data->comment : '',
			);
		}
		Excel::create('Customers', function ($excel) use ($column_array) {
			$excel->setTitle('Customer Data');
			$excel->sheet('Customer Data', function ($sheet) use ($column_array) {
				$sheet->fromArray($column_array, null, 'A1', false, false);
			});
		})->store('xlsx', storage_path('app/public/exports'));

		return response()->json(['status' => true, 'url' => url('storage/exports/Customers.xlsx')]);
	}
	public function demoExport() {
		$column_array[] = array('Contractor', 'Skill', 'Skillcontact', 'Office', 'Mobile', 'Email', 'Comments');
		$column_array[] = array('RWTDesign', 'DesignBuildFirms', 'test', '8163436065', '8167212529', 'exmple@gmail.com', 'test');
		Excel::create('Customer Data', function ($excel) use ($column_array) {
			$excel->setTitle('Customer Data');
			$excel->sheet('Customer Data', function ($sheet) use ($column_array) {
				$sheet->fromArray($column_array, null, 'A1', false, false);
			});
		})->download('xlsx');
	}
}
