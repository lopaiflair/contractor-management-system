<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Response
     */
   /* public function index()
    {
        return view('admin.dashboard')->with('contentheader_title', "Dashboard");
    }*/
    public function index(){
        
        return view('admin.homepage')->with('contentheader_title', "Dashboard");
    }
}
