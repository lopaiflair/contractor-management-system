<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Cookie;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'dashboard';

    /**
     * Shows the admin login form
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
       
        if (auth()->guard('admins')->check()) {

            return redirect('profile');
        }

        return view('adminlte::auth.login');
    }

    /**
     * Login the employee
     *
     * @param LoginRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {

        $this->validateLogin($request);

        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        $details = $request->only('email', 'password');
        if (auth()->guard('admins')->attempt($details)) {

            if ($request->has('remember')) {
                Cookie::queue('remember_me', "true", 360000);
                Cookie::queue('email', $request->email, 360000);
                Cookie::queue('password', $request->password, 360000);
            } else { 
                Cookie::queue(Cookie::forget('remember_me'));
                Cookie::queue(Cookie::forget('email'));
                Cookie::queue(Cookie::forget('password'));
            }

            if (auth()->guard('admins')->user()->role == "User") { echo 'user';
                \App\LoginHistory::Create(['user_id' => auth()->guard('admins')->user()->id, 'action' => 'Login']);
            }
            // return redirect('dashboard');
            return $this->sendLoginResponse($request);
        }
        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.

        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        if (auth()->guard('admins')->user()->role == "User") {
            \App\LoginHistory::Create(['user_id' => auth()->guard('admins')->user()->id, 'action' => 'Logout']);
        }

        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/login');
    }

    public function show_profile(Request $request)
    {
        $breadcrum = array();
        $breadcrum[0]['name'] = "Profile";
        $breadcrum[0]['url'] = url('profile');

        return view('admin.profile', compact('breadcrum'))->with('contentheader_title', 'Profile');
    }

    public function update_profile(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $user_obj = \App\Admin::findOrFail(auth()->guard('admins')->user()->id);

        if (!empty($user_obj)) {

            $user_obj->name = $request->name;

            if ($request->hasFile('profile_picture')) {
                \Storage::delete('public/profile_image/' . $user_obj->profile_picture);
                $image = $request->file('profile_picture');
                $name = time() . '.' . $image->getClientOriginalExtension();
                \Storage::disk('local')->putFileAs('public/profile_image', $image, $name);
                $user_obj->profile_picture = $name;
            }

            if (!empty($request->password)) {
                $user_obj->password = bcrypt($request->password);
            }

            $user_obj->save();

            return redirect('profile')->with('success', "Profile updated successfully.");
        } else {
            return redirect('profile')->with('error', "Failed to Update Profile");
        }
    }
    
}
