<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Scheduler;
use App\Skill;
use DB;
use \Carbon\Carbon;
use Telnyx;
/**
 * Class DashboardController
 * @package App\Http\Controllers
 */
class ScheduleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->time_slot_array = array(
            "10:00-10:30" => "5837 Tracy Kansas City, Missouri 64110",
            "10:45-11:15" => "3406 Michigan Kansas City, Missouri 64109",
            "11:45-12:15" => "4218 Virginia Kansas City, Missouri 64110",
            "12:30-13:00" => "4216 Virginia Kansas City, Missouri 64110",
            "13:15-13:45" => "2637 Garfield Kansas City, Missouri 64127",
            //"02:00-02:30" => "4216 Virginia KCMO 64110",
           // "02:45-03:15" => "703 East 43rd Street KCMO 64110"
        );
        $this->time_conversation = array(
            "10:00-10:30" => "10:00am - 10:30am",
            "10:45-11:15" => "10:45am - 11:15am",
            "11:45-12:15" => "11:45am - 12:15pm",
            "12:30-01:00" => "12:30pm - 01:00pm",
            "01:15-01:45" => "01:15pm - 01:45pm",
            "02:00-02:30" => "02:00pm - 02:30pm",
            "02:45-03:15" => "02:45pm - 03:15pm"
        );
         $this->time_formet = array(
            "10:00-10:30" => "10:00 am – 10:30 am",
            "10:45-11:15" => "10:45 am – 11:15 am",
            "11:45-12:15" => "11:45 am – 12:15 pm",
            "12:30-13:00" => "12:30 pm – 1:00 pm",
            "13:15-13:45" => "1:15 pm – 1:45 pm",
        );
        //$this->middleware('auth');
    }

    function schedule_select(){
         return view('scheduler.index')->with('contentheader_title', "Scheduler");
    }
    function schedule_select_save(Request $request){
 
        $timestamp = strtotime($request->schedule_date);
        $fullday=date('l', $timestamp);
        $month=date('F', $timestamp);
        $year=date('Y', $timestamp);
        $date=date('d', $timestamp);
         $available_slot=$request->available_slot;
        $mins = 30;
        $available_slot_ex = (explode("-",$available_slot));
        $start_time_12=date('h:ia', strtotime($available_slot_ex['0']));
        $end_time_12=date('h:ia', strtotime($available_slot_ex['1']));

        $date_string=$start_time_12." - ".$end_time_12.", ".$fullday.", ".$month." ".$date.", ".$year;

        $request->session()->put('scheduler_schedule_date', $request->schedule_date);
        $request->session()->put('scheduler_timeslot',$request->available_slot);
        $request->session()->put('scheduler_fulldate',$date_string); 
        $request->session()->put('scheduler_total_mins',$mins); 

        $skills = Skill::where('status',1)->orderBy('skill','asc')->get()->toarray();
        $add_conversation_array=$this->time_slot_array;
        $address=$add_conversation_array[$request->available_slot];
        $request->session()->put('scheduler_address',$address); 
 
          return view('scheduler.create')->with('contentheader_title', "Schedule List
")->with('skills',$skills)->with('date_string',$date_string);
     }
     public function store(Request $request){
                                  

         $this->validate($request, [
          'name' => ['required', 'string', 'max:255'],
          'email' => ['required'],
          'mobile_number' => ['required'],
          'skill_set' => ['required'],
         ]);

                                 

        if(!empty($request->skill_set)){ 
            $skill_set     = implode(',', $request->skill_set);
        }else{
            $skill_set     = '';
            
        }

        $skill_names = Skill::select('skill')
        ->whereIn('id', $request->skill_set)
        ->pluck('skill')->implode(', ');

        $schedule_date=session('scheduler_schedule_date', 'default');
        $timeslot=session('scheduler_timeslot', 'default');
        $orgDate = $schedule_date;  
        $newDate = date("Y-m-d", strtotime($orgDate));
        $timeformet_arrray=$this->time_formet;
        $time_formet=$timeformet_arrray[$timeslot];


 
        \Telnyx\Telnyx::setApiKey(env('TELNYX_KEY'));
                        $info = \Telnyx\Message::Create([
                             'messaging_profile_id' => env('MESSAGE_PROFILE_ID'),
                            //'from' => '+18162083113',
                            'to'   => $request->mobile_number,
                            'text' => "Hello $request->name. You are scheduled to meet Faith Properties $schedule_date at $time_formet for $skill_names project.",
                        ]);

           $category = Scheduler::create([
            'schedule_date' =>$newDate,
            'timeslot' => $timeslot,
            'name' => $request->name,
            'email' => $request->email,
            'what_address_you_are_coming_to' =>session('scheduler_address', 'default'),
            'mobile_number' => $request->mobile_number,
            'skill_set' => $skill_set,
         ]);
          return redirect()->route('dashboard')
            ->with('success', 'Scheduler created successfully');
    }
    public function checkemail(Request $request){

        $email  = $request->email;
       
        $email_count = Scheduler::where('email', '=', $email)
                             ->count();
            if($email_count > 0){
                return 'false';
            }else{
                return 'true';
            }
       
        
    }
    
}
