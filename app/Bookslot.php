<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bookslot extends Model
{
    protected $table = 'contractor_schedules';
     protected $fillable = [
        'schedule_date',
        'timeslot',
        'name',
        'email',
        'what_address_you_are_coming_to',
        'mobile_number',
        'skill_set',
        'sent_text_message_to',
        'notes',
        'schedule_message',
        'schedule_message_sent',
        'hired',
        'reminder',
        'reminder_message',
        'reminder_interval',
        'reminder_sent_on',
        'created_at',
        'updated_at',
    ];
}
