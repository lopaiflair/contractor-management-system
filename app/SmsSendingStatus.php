<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsSendingStatus extends Model
{
    protected $fillable = ['from_number', 'to_number', 'message', 'status', 'response'];
}
