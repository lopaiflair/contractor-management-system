<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scheduler extends Model
{
    protected $table = 'scheduler';
     protected $fillable = [
        'schedule_date',
        'timeslot',
        'name',
        'email',
        'what_address_you_are_coming_to',
        'mobile_number',
        'skill_set',
        'reminder_sent_on',
        'created_at',
        'updated_at',
    ];
}
