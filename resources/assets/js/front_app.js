try {
  window.$ = window.jQuery = require('jquery');
} catch (e) {}

require('bootstrap');
require('bootstrap-datepicker');
window.toastr = require('toastr');
require('icheck');
require('jquery-validation');

window.signaturePad = require('signature_pad');
window.moment = require('moment');
