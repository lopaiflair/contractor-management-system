@extends('adminlte::layouts.app')

@section('htmlheader_title',"Addresses")

@section('main-content')

	<div class="container-fluid spark-screen all_contractor">
		<div class="row">
			<div class="col-xs-12">
				@if ($message = Session::get('success'))
                <div class="success_msg">
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="error_msg">
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">

				<!-- Default box -->
				<div class="box main-section">
					<div class="box-body table-responsive no-padding">
					    <div class="defalt-btbox-div">
					    	<?php $getCount =  DB::table('contractors')->count();?>
					    	<div class="col-xs-6">
	                            <span><h1>Total Contractors: <?php echo $getCount;?></h1></span>
	                        </div>
					    	<div class="col-xs-6">
	                            <a href="{{url('contractors/create')}}" class="add_address_btn" data-placement="top" data-toggle="tooltip" data-original-title="Add Contractor">Add Contractors</a>
	                            <a href="{{url('contractors/import')}}" class="import_addresses_btn" data-placement="top" data-toggle="tooltip" data-original-title="Import Contractors">Import Contractors</a>
	                            <a class="import_addresses_btn checkexport" data-placement="top" data-toggle="tooltip" data-original-title="Import Contractors">Export Contractors</a>
                        	</div>
                             

                       </div>

                        <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="UserTable">
		                    <thead>
		                        <tr>
		                        	<th style="display:none;">ID</th>
		                            <th>Name</th>
		                            <th>Email</th>
		                            <th>Skill</th>
		                            <th>Office</th>
		                            <th>Mobile</th>
		                            <th class="th_disable">Action</th>
		                        </tr>
		                    </thead>

		                    

			            </table>
		        	</div><!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
@endsection
	@section('javascripts')
	<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
	<script type="text/javascript">
		$(function($){
			setTimeout(function() {
		        $(".success_msg").hide('blind', {}, 300);
		        $(".error_msg").hide('blind', {}, 300);

		    }, 5000);
			var dataTable = $('#UserTable').DataTable( {
			  	"processing": true,
				"serverSide": true,
				"ajax":{
	                "url": '{{url("contractors/get_datatable_data")}}',
	                "dataType": "json",
	                "type": "POST",
	                "data":{ '_token': "{{csrf_token()}}"}
	             },
	            "method":'post',
			  	"columns": [
			  		{ "data": "id", "name":"id"},
				    { "data": "name", "name":"name"},
				    { "data": "email", "name":"email"},
				    { "data": "skillname", "name":"skillname"},
				    // { "data": "skill_contact", "name":"skill_contact"},
				    { "data": "office", "name":"office"},
				    { "data": "mobile_no", "name":"mobile_no"},
				    
				    // { "data": "comment", "name":"comment"},
				    // { "data": "created_at", "name":"created_at"},
				    { "data": "id",
			            "render": function(id, type, row ){
			                   /*return '<a class="btn btn-lg" data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="contractors/edit/'+id+'">' + '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>' + '</a><form action="{{ url("contractors/delete") }}/'+id+'" method="POST" onsubmit="return confirm(\'Are you sure you want to delete this record?\')">{{ csrf_field() }}{{ method_field("DELETE") }}<button type="submit" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button></form>';*/
			                   return '<a class="editbtn" data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="contractors/edit/'+id+'">Edit</a> <a href="{{ url("contractors/delete") }}/'+id+'" onclick="return confirm('+"'Are you sure you want to delete this record?'"+');" class="deletebtn" data-toggle="tooltip" data-original-title="Delete">Delete</a>';
			            }
			            ,searchable: false, sortable: false
				    },
				],
				"order": [ 0, 'desc' ],
	        	"columnDefs": [
		            {
		                "targets": [ 0 ],
		                "visible": false,
		                "searchable": false
		            }
	        	]
			});
		});

		 $(document).on('click','.checkexport', function() {
		 	var search = $('.dataTables_filter input').val();
            $.ajax({
                url: "{{url('contractors/export')}}",
                type: "post",
             	data: { search : search},
                headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                success:function(response){
	                var a = document.createElement("a");
	                a.href = response.url;
	                a.download = "customers.xlsx";
	            	a.click();
	            	a.remove();
                }
            });
        });

		$('#UserTable').on('search.dt', function() {
		var value = $('.dataTables_filter input').val();
		}); 
	</script>
	@endsection
