@extends('adminlte::layouts.app')

@section('htmlheader_title',"Add Address")

@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-xs-12">
                @if ($message = Session::get('success'))
                <div>
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div>
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif

                <div class="col-xs-12 box add-padbox">
                    <form method="POST" action="{{url('contractors/store')}}" id="myform" enctype="multipart/form-data">

                        @csrf
                        <div class="col-lg-6">
                            <?php 
                                $name = '';
                                $email= '';
                               // $skill= '';
                                $office = '';
                                $mobileno ='';
                                $comment = '';
                                $c_id    = '';
                                $skill_contact = '';
                                if(!empty($editeData)){
                                    $name  = !empty($editeData->name) ? $editeData->name : '';
                                    $email  = !empty($editeData->email) ? $editeData->email : '';
                                    //$skill  = !empty($editeData->skills) ? $editeData->skills : '';
                                    $mobileno  = !empty($editeData->mobile_no) ? $editeData->mobile_no : '';
                                    $office  = !empty($editeData->office) ? $editeData->office : '';
                                    $comment  = !empty($editeData->comment) ? $editeData->comment : '';
                                    $c_id  = !empty($editeData->id) ? $editeData->id : '';
                                    $skill_contact = !empty($editeData->skill_contact) ? $editeData->skill_contact : '';
                                    
                                }?>

                            <?php if(!empty($editeData)){?>
                                <input type="hidden" name="contractor_id" value="<?=$editeData->id?>" id="contractor_id">
                            <?php } ?>
                                <input type="hidden" name="c_id" value="<?=$c_id?>" id="c_id">
                            <div class="form-group">
                                <label for="name">Name </label>
                                <input class="form-control" name="name" type="text" id="name" value="<?=$name?>" placeholder="Name">
                                
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group">
                                <label for="email">Email <!-- <span class="asterisk">*</span> --></label>
                                <input class="form-control" name="email" type="email" id="email" value="<?=$email?>" placeholder="Email">
                               
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                                <?php 
                                    $skillArr = [];
                                    if(!empty($editeData)){
                                      $skillArr = explode(',', $editeData->skills);
                                    }
                                    
                                ?>
                                <label for="Skill">Skill</label>
                                <select name="skill[]" class="form-control" id="skills" multiple="multiple">
                                    <!-- <option value="">Please select skill</option> -->
                                    <?php 
                                        
                                        
                                        if(!empty($skills)){
                                            foreach ($skills as $key => $value) { ?>
                                                <option value="<?=$value['id']?>"
                                                    <?php if(!empty($skillArr)){
                                                        if(in_array($value['id'],$skillArr)){
                                                            echo "selected";
                                                        }

                                                    }?>
                                                ><?=$value['skill']?></option>
                                    <?php   }
                                        }
                                    ?>
                                </select>
                               
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group">
                           <!--  <button type="button" class="btn-faith addQuick" data-toggle="modal" data-target="#myModal">Add</button> -->
                            <button type="button" class="btn-faith addQuick">Add</button>
                            </div>
                            <!-- <a href="{{url('skills')}}" target="_blank">
                                <button type="button" class="btn-faith">View</button>
                            </a> -->
                        </div>
                        <div class="col-lg-6">
                             <div class="form-group">
                                <label for="skill_contact">Skill Contact</label>
                                <input class="form-control" name="skill_contact" type="text" id="skill_contact" value="<?=$skill_contact?>" placeholder="Skill Contact">
                               
                            </div>
                        </div>
                        <div class="col-lg-6">
                             <div class="form-group">
                                <label for="office">Office <!-- <span class="asterisk">*</span> --></label>
                                <input class="form-control" name="office" type="text" id="office" value="<?=$office?>" placeholder="Office">
                               
                            </div>
                        </div>
                        <div class="col-lg-6">
                             <div class="form-group">
                                <label for="mobileno">Mobile Number <!-- <span class="asterisk">*</span> --></label>
                                <input class="form-control" name="mobileno" type="text" id="mobileno" value="<?=$mobileno?>" placeholder="Mobile no">
                                
                            </div>
                        </div>
                        <div class="col-lg-6">
                              <div class="form-group">
                                <label for="comment">Comment</label>
                                <input class="form-control" name="comment" type="text" id="comment" value="<?=$comment?>" placeholder="Comment">
                              
                            </div>
                        </div>

                        <div class="col-lg-6">
                        <div class="form-group">
                            <input type="submit" value="Submit" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                            <a href="{{url('contractors')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
<div id="myModal" class="modal fade contractor_modal" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add skill</h4>
      </div>
      <div class="modal-body">
            <!-- <form method="POST" action="" id="skillform"> -->

                <div>
                    <label for="addskill">Skill </label>
                    <input class="form-control" name="addskill" type="addskill" id="addskill" value="" placeholder="Skill">
                    <br/>
                </div>
                
                 <div>
                    <input type="submit" value="Save" class="btn-faith addnewskill" data-toggle="tooltip" data-original-title="Submit">
                </div>
                         
         <!--    </form> -->
      </div>
     
    </div>

  </div>
</div>
    @section('javascripts')
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script type="text/javascript">
        $(function() {
            $('#skills').select2();
            jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });

             
            $('.addnewskill').click(function(){
                var skill = $('#addskill').val();
                if(skill != ''){
                    $.ajax({
                       type: "post",
                       url: "{{url('contractors/addnewskill')}}",
                       data: {
                              "newskill": $('#addskill').val(),
                              "_token" : "<?php echo csrf_token(); ?>"
                            },  
                       success: function(data)
                       {
                          var optiondata = jQuery.parseJSON(data);
                          $("#myModal").modal("hide");
                          $('#skills').append('<option value="'+optiondata.id+'">'+optiondata.skill+'</option>'); 
                       }
                 });
                }else{
                    alert('Please enter skill');
                }
                
            });
             
            jQuery.validator.addMethod("maxLen", function (value, element, param) {
                //console.log('element= ' + $(element).attr('name') + ' param= ' + param )
                if ($(element).val().length > param) {
                    return false;
                } else {
                    return true;
                }
            }, "Please enter no more than 10 digits.");

            $("#myform").validate({

                rules: {
                    email: {
                        email:true,
                        // checkExists:true
                         remote: {
                            url: "{{url('contractors/checkemail')}}",
                            type: 'post',
                            data: {
                              "_token" : "<?php echo csrf_token(); ?>",
                              "c_email": $('#email').val(),
                              'contractor_id' :$('#c_id').val()
                              
                            } 

                        },
                       
                       
                    },
                    office: {
                        digits: true,
                        maxLen: 10
                    },
                    mobileno: {
                        digits: true,
                        maxLen:10,
                        remote: {
                            url: "{{url('contractors/checkmobile')}}",
                            type: 'post',
                            data: {
                              "_token" : "<?php echo csrf_token(); ?>",
                              "c_mobile": $('#mobileno').val(),
                              'contractor_id' :$('#c_id').val()
                              
                            } 

                        },                       
                    }
                },
                messages: {
                    email: {
                        "email" : "Please enter a valid email address.",
                        "remote": "This email is already exist."
                       
                    },
                    digits: {
                        "required": "Please enter digits only.",
                      
                    },
                    mobileno: {
                        "digits": "Please enter digits only.",
                        "remote": "This mobile number is already exist."
                       
                    }
                }
            });
            var skillval = '';
            $('.select2-search__field').keyup(function(){
                skillval = $(this).val();
                
            });
            
            $('.addQuick').click(function(){
                if(skillval != ''){
                    $.ajax({
                       type: "post",
                       url: "{{url('contractors/addquickskill')}}",
                       data: {
                              "newskill": skillval,
                              "_token" : "<?php echo csrf_token(); ?>"
                            },  
                       success: function(data)
                       {
                          var optiondata = jQuery.parseJSON(data);
                              if(optiondata.message == 'success'){
                                $('#skills').empty();
                                $('#skills').select2();
                                $.each(optiondata.skillArr, function(key, value) { 
                                    if(value.skill == optiondata.skill){
                                        $('#skills').append($("<option></option>").attr("value",key).attr('selected','selected').text(value.skill));
                                    }else{
                                        $('#skills').append($("<option></option>").attr("value",key).text(value.skill));
                                    }
                                     
                                });
                                
                              }
                        }
                      
                 });
                }
                // alert(skillval);
            });
        });
    </script>
    @endsection
@endsection
