@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-xs-12" id="msg">
				@if ($message = Session::get('success'))
                <div class="success_msg">
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="error_msg">
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-body">
						<div class="box-header with-border">
							<img src="{{url('public/img/front_logo.png')}}" alt="Faith Properties" width="42%">
						</div>
						<div>
							<h1>
								Contractor Management System
							</h1>
						</div>

						 
						<div>
							<a href="{{url('contractors')}}">
								<h3>
									Add Contractor (s)
								</h3>
							</a>
						</div>
						<!-- <div>
							<a href="{{url('skills')}}">
								<h3>
									Add Skill (s)
								</h3>
							</a>
						</div> -->
						<div>
							<a href="{{url('invite')}}">
								<h3>
									Invite Contractor (s) to Bid
								</h3>
							</a>
						</div>

						<div>
							<a href="{{url('bookslot')}}">
								<h3>
									Schedule contractors
								</h3>
							</a>
						</div>
						<div>
							<a href="{{url('bookslot/schedule_contractors_list')}}">
								<h3>
									Schedule contractors list
								</h3>
							</a>
						</div>
						<div>
							<a href="{{url('scheduler/schedule_select')}}">
								<h3>
									Scheduler
								</h3>
							</a>
						</div>
					</div>
					
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
	@section('javascripts')
	<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
	<script type="text/javascript">
		var canvas = document.querySelector("canvas");

		var signaturePad = new SignaturePad(canvas);
		$("#save").on('click',function(){
			var data = signaturePad.toDataURL('image/png');
			console.log(data);
			window.open(data);
		})

		$(".date_picker").datepicker();

		$(".daterange_picker").daterangepicker();

		$(".color_picker").colorpicker();

		$(".time_picker").timepicker();

		$(document).ready(function () {
			CKEDITOR.replace('ck');
		});

		$(".datatable").DataTable();
	</script>
	@endsection
@endsection
