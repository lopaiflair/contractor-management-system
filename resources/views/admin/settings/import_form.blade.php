@extends('adminlte::layouts.app')

@section('htmlheader_title',"Add Contractor")

@section('main-content')
    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-xs-12">
                @if ($message = Session::get('success'))
                <div class="success_msg">
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="error_msg">
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif

                <div class="col-xs-12 box add-padbox">
                    <form method="POST" action="{{url('contractors/importdata')}}" id="myform" enctype="multipart/form-data">

                        @csrf

                        <div class="form-group">
                            <label for="import_csv">Import File (CSV and XLSX Only) <span class="asterisk">*</span></label>
                            <input type="file" name="import_csv" class="form-control">
                            @if ($errors->has('import_csv'))
                            <span class="help-block">
                                <strong class="errors">{{ $errors->first('import_csv') }}</strong>
                            </span>
                            @endif
                        </div>

                        <div class="">
                            <input type="submit" value="Submit" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                            <a href="{{url('contractors')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @section('javascripts')
    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
    <script type="text/javascript">
        $(function() {
            setTimeout(function() {
                $(".success_msg").hide('blind', {}, 300);
                $(".error_msg").hide('blind', {}, 300);

            }, 5000);
            jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });

            $("#myform").validate({
                ignore: [],
                rules: {
                    import_csv: {
                        required: true,
                        extension: "csv|xlsx"
                    },
                },
                messages: {
                    import_csv: {
                        required: "This field is required.",
                        extension: "This field allows only excel file(.csv,.xlsx)."
                    },
                }
            });
        });
    </script>
    @endsection
@endsection
