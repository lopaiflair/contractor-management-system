@extends('adminlte::layouts.app')

@section('htmlheader_title',"Add Address")

@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-xs-12">
                @if ($message = Session::get('success'))
                <div>
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div>
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif

                <div class="col-xs-12 box add-padbox">
                    <form method="POST" action="{{url('setting/store')}}" id="myform" enctype="multipart/form-data">

                        @csrf
                        <div class="col-lg-12">
                              
                            <?php 
                                $mobileNo = !empty($seting_data) ? $seting_data->mobileno: '';
                                $setting_id = !empty($seting_data) ? $seting_data->id: '';
                            ?>
                            <input type="hidden" name="setting_id" value="<?=$setting_id?>">
                            <div class="form-group">
                                <label for="mobileno">Mobile No </label>
                                <input class="form-control" name="mobileno" type="text" id="mobileno" value="<?=$mobileNo?>" placeholder="Mobile No">
                                
                            </div>
                        </div>
                       
                        <div class="col-lg-12">
                            <div class="form-group">
                                
                                <input class="" name="status" type="checkbox" id="status" value="1" <?php 
                                if(!empty($seting_data)){
                                if($seting_data->status == 1){ 
                                    echo "checked";
                                } } ?>>
                                <label for="status"> Set as default</label>
                            </div>
                        </div>

                       <div class="col-lg-12">
                        <div class="form-group">
                            <input type="submit" value="Submit" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                            <a href="{{url('setting')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @section('javascripts')
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> -->
<!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script> -->
    <script type="text/javascript">
        $(function() {
             jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });
             jQuery.validator.addMethod("maxLen", function (value, element, param) {
                //console.log('element= ' + $(element).attr('name') + ' param= ' + param )
                if ($(element).val().length > param) {
                    return false;
                } else {
                    return true;
                }
            }, "Please enter no more than 10 digits.");
            $("#myform").validate({

                rules: {
                  
                    mobileno: {
                        required:true,
                         maxLen:10,
                        digits:true,
                    }
                },
                messages: {
                   
                    mobileno: {
                        "required": "Please enter number."
                       
                    }
                }
            });
          
        });
    </script>
    @endsection
@endsection
