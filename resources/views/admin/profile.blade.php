@extends('adminlte::layouts.app')

@section('htmlheader_title','Profile')

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-xs-12">
            @if ($message = Session::get('success'))
            <div class="success_msg">
                <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
            </div>
            @endif
            @if ($message = Session::get('error'))
            <div class="error_msg">
                <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
            </div>
            @endif
            <!-- Default box -->
            <div class="">
                <div class="admin-user col-xs-12 box add-padbox">
                    <form method="POST" action="{{url('profile')}}" id="myform" enctype="multipart/form-data">
                        @csrf
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 profile-page">
                            <div class="unique-text">
                            </div>
                            <div id="profile-image">
                                @if(!empty(auth()->guard('admins')->user()->profile_picture))
                                <a data-fancybox="gallery" href="{{ asset('storage/profile_image/'.auth()->guard('admins')->user()->profile_picture)}}" id="fancybox_anchor">
                                    <div id="profile_pic_div" style="background-image: url('{{ asset('storage/profile_image/'.auth()->guard('admins')->user()->profile_picture)}}')"></div>
                                </a>
                                @else
                                <a data-fancybox="gallery" href="{{ asset('img/noimage.png') }}" id="fancybox_anchor">
                                    <div id="profile_pic_div" style="background-image: url('{{ asset('img/noimage.png') }}')"></div>
                                </a>
                                @endif
                                <a id="change-picture" href="javascript:void(0)">Change Picture</a>
                                <input id="profile-image-upload" class="hidden" name="profile_picture" onchange="readURL(this)" type="file" />
                            </div>
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="form-group">
                                <label for="last_name">Name <span class="asterisk">*</span></label>
                                <input class="form-control" name="name" type="text" id="name" value="{{ old('name',auth()->guard('admins')->user()->name)}}" placeholder="Name">
                                @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong class="errors">{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="email">Email <span class="asterisk">*</span></label>
                                <input class="form-control" name="email" readonly="true" type="text" id="email" value="{{ old('email',auth()->guard('admins')->user()->email)}}" placeholder="{{trans('labels.email')}}">
                                @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong class="errors">{{ $errors->first('email') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group rit-padingbox">
                                <label for="password">Password </label>
                                <input class="form-control" name="password" type="password" id="password" placeholder="Password">
                                <i class="fa fa-eye password_field" id="toggle" aria-hidden="true"></i>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong class="errors">{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group rit-padingbox">
                                <label for="password">Confirm Password </label>
                                <input class="form-control" name="password_confirm" type="password" id="password_confirm" placeholder="Confirm Password">
                                <i class="fa fa-eye password_field" id="toggle_cpnf_pass" aria-hidden="true"></i>
                                @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong class="errors">{{ $errors->first('password') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="">
                                <input type="submit" value="Submit" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                                <a href="{{url('dashboard')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- /.box -->

        </div>
    </div>
</div>
@section('javascripts')
    <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.js"></script>
<script type="text/javascript">
    $(function() {
        setTimeout(function() {
                $(".success_msg").hide('blind', {}, 300);
                $(".error_msg").hide('blind', {}, 300);

            }, 5000);
        jQuery.validator.setDefaults({
            debug: false,
            success: "valid"
        });

        $.validator.addMethod("pwcheck", function(value) {
            return /[!@#$%^&*()]+/.test(value) &&
                /[\d\W]+/.test(value);
        });

        $.validator.addMethod(
            "checkimage",
            function(value, element) {
                if ($(element).attr('type') == "file" && ($(element).hasClass('required') || $(element).get(0).value.length > 0))
                    return value.match(/\.(jpg|png|JPG|PNG|GIF|gif|jpeg|JPEG])$/i);
                else
                    return true;
            }, "Profile image must be type of one of these jpg, png, gif, jpeg");

        jQuery.validator.addMethod("noSpace", function(value, element) {
            return value.indexOf(" ") < 0;
        }, "This field should not allow spaces");

        $("#myform").validate({
            ignore: [],
            rules: {
                "name": {
                    required: true,
                    noSpace: true
                },
                /*"password" : {
                    minlength : 7,
                    "pwcheck":
                        {
                           depends: function(element){
                               if($("#password").val()){
                                   var status = true;
                               }
                               else{
                                    var status = false;
                               }
                               return status;
                           }
                        }
                },*/
                "password_confirm": {
                    "equalTo": "#password",
                },
                "profile_picture":{
                    checkimage:{
                        depends: function(element){
                            if(element.value){
                                return true;
                            }
                        }
                    },
                }
            },
            messages: {
                "first_name": {
                    "required": "The First Name field is required."
                },
                "last_name": "The Last Name field is required.",
                "password": {
                    "pwcheck": "Please make password strong using any special character and number.",
                },
                'password_confirm': {
                    "equalTo": "Confirm password must match with password."
                }
            },
            errorPlacement: function(error, element) {
                if (element.attr("name") == "profile_picture") {
                    error.insertAfter("#profile-image");
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('#change-picture').on('click', function() {
            $('#profile-image-upload').click();
        });

        $("#toggle").on('click', function() {
            var passwordField = document.getElementById('password');
            var value = passwordField.value;

            if (passwordField.type == 'password') {
                passwordField.type = 'text';
            } else {
                passwordField.type = 'password';
            }
            passwordField.value = value;
        });

        $("#toggle_cpnf_pass").on('click', function() {
            var passwordField = document.getElementById('password_confirm');
            var value = passwordField.value;

            if (passwordField.type == 'password') {
                passwordField.type = 'text';
            } else {
                passwordField.type = 'password';
            }
            passwordField.value = value;
        });

    });

    function readURL(input) {
        var ext = input.files[0]['name'].substring(input.files[0]['name'].lastIndexOf('.') + 1).toLowerCase();
        if (input.files && input.files[0] && (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('#profile_pic_div').css('background-image', "url('" + e.target.result + "')");
                $('#fancybox_anchor').attr('href', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }

</script>
@endsection
@endsection
