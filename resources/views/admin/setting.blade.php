@extends('adminlte::layouts.app')

@section('htmlheader_title','Profile')

@section('main-content')
<div class="container-fluid spark-screen">
    <div class="row">
        <div class="col-xs-12">
            @if ($message = Session::get('success'))
            <div>
                <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
            </div>
            @endif
            @if ($message = Session::get('error'))
            <div>
                <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
            </div>
            @endif
            <!-- Default box -->
            <div class="">
                <div class="box add-padbox">
                    <form method="POST" action="{{ action('Admin\SettingController@update',$setting->id)}}" id="myform" enctype="multipart/form-data">
                        <div class="box-body">
                            @csrf
                            @method('PUT')
                            <div class="form-group col-lg-12">
                                <label for="email_content">Email Content <span class="asterisk">*</span></label>
                                <textarea name="email_content" id="email_content">{{ old('email_content',$setting->email_content)}}</textarea>
                                @if ($errors->has('email_content'))
                                <span class="help-block">
                                    <strong class="errors">{{ $errors->first('email_content') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="sms_content">SMS Content <span class="asterisk">*</span></label>
                                <textarea name="sms_content" id="sms_content">{{ old('sms_content',$setting->sms_content)}}</textarea>
                                @if ($errors->has('sms_content'))
                                <span class="help-block">
                                    <strong class="errors">{{ $errors->first('sms_content') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="form-group col-lg-12">
                                <label for="report_notification_content">Report Notification Content <span class="asterisk">*</span></label>
                                <textarea name="report_notification_content" class="form-control">{{ old('report_notification_content',$setting->report_notification_content)}}</textarea>
                                @if ($errors->has('report_notification_content'))
                                <span class="help-block">
                                    <strong class="errors">{{ $errors->first('report_notification_content') }}</strong>
                                </span>
                                @endif
                            </div>

                            <div class="form-group col-lg-12">
                                <label for="sms_content">Send Email After <span class="asterisk">*</span></label>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <input type="number" name="send_email_after_days" class="form-control sendafter-email-group" placeholder="Days" min="0" value="{{old('send_email_after_days',$setting->send_email_after_days)}}">
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="number" name="send_email_after_hours" class="form-control sendafter-email-group" placeholder="Hours" min="0" max="23" value="{{old('send_email_after_hours',$setting->send_email_after_hours)}}">
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="number" name="send_email_after_minutes" class="form-control sendafter-email-group" placeholder="Minutes" min="0" max="59" value="{{old('send_email_after_minutes',$setting->send_email_after_minutes)}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-lg-12">
                                <label for="sms_content">Send SMS After <span class="asterisk">*</span></label>
                                <div class="row">
                                    <div class="col-lg-4">
                                        <input type="number" name="send_sms_after_days" class="form-control sendafter-sms-group" placeholder="Days" min="0" value="{{old('send_sms_after_days',$setting->send_sms_after_days)}}">
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="number" name="send_sms_after_hours" class="form-control sendafter-sms-group" placeholder="Hours" min="0" max="23" value="{{old('send_sms_after_hours',$setting->send_sms_after_hours)}}">
                                    </div>
                                    <div class="col-lg-4">
                                        <input type="number" name="send_sms_after_minutes" class="form-control sendafter-sms-group" placeholder="Minutes" min="0" max="59" value="{{old('send_sms_after_minutes',$setting->send_sms_after_minutes)}}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group col-lg-12 inline-iconsbox">
                                <label for="ph_numbers_to_send_notification">Notify to</label>
                                <input type="text" class="form-control" name="ph_numbers_to_send_notification[]"/>
                                <i class="fa fa-plus add" aria-hidden="true"></i>
                            </div>
                            @if(!empty($setting->ph_numbers_to_send_notification))
                            <?php $phone_numbers = array_map('trim', explode(",", $setting->ph_numbers_to_send_notification));?>
                                @foreach($phone_numbers as $ph)
                                    <div class="form-group col-lg-12 inline-iconsbox">
                                        <input type="text" class="form-control" name="ph_numbers_to_send_notification[]" value="{{$ph}}"/><i class="fa fa-trash remove" aria-hidden="true"></i>
                                    </div>
                                @endforeach
                            @endif
                            <div class="notify"></div>

                            <div class="form-group col-lg-12 inline-iconsbox">
                                <input type="checkbox" class="form-control" name="report_notification" @if($setting->report_notification == "On"){{"checked"}}@endif/>
                                <label for="report_notification">Report Notification</label>
                            </div>

                            <div class="form-group col-lg-12">
                                <input type="submit" value="Submit" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                            </div>

                        </div>
                    </form>
                </div>
                <!-- /.box -->

            </div>
        </div>
    </div>
</div>
@section('javascripts')
<script type="text/javascript">
    $(function() {
        $("input").iCheck({
            checkboxClass: 'icheckbox_square-yellow',
        });

        CKEDITOR.replace('email_content', {
            allowedContent: true,
        });
        CKEDITOR.replace('sms_content', {
            allowedContent: true,
        });
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].on('change', function() {
                var editorName = $(this)[0].name;
                CKEDITOR.instances[editorName].updateElement();
            });
        }

        jQuery.validator.setDefaults({
            debug: false,
            success: "valid"
        });

        $("#myform").validate({
            ignore: [],
            rules: {
                email_content: {
                    required: true
                },
                sms_content: {
                    required: true
                },
                report_notification_content: {
                    required: true
                },
                send_email_after_day: {
                    require_from_group: [1, ".sendafter-email-group"]
                },
                send_email_after_hours: {
                    require_from_group: [1, ".sendafter-email-group"]
                },
                send_email_after_minute: {
                    require_from_group: [1, ".sendafter-email-group"]
                },
                send_sms_after_day: {
                    require_from_group: [1, ".sendafter-sms-group"]
                },
                send_sms_after_hours: {
                    require_from_group: [1, ".sendafter-sms-group"]
                },
                send_sms_after_minute: {
                    require_from_group: [1, ".sendafter-sms-group"]
                }
            },
            messages: {
                email_content: {
                    "required": "The Email Content field is required."
                },
                sms_content: {
                    "required": "The SMS Content field is required."
                }
            },
            errorPlacement: function (error, element) {
                if (element.attr("name") == "email_content") {
                    error.insertBefore($(element));
                    error.addClass('ckeditor_error');
                }
                else if (element.attr("name") == "sms_content") {
                    error.insertBefore($(element));
                    error.addClass('ckeditor_error');
                }
                else{
                    error.insertAfter($(element));
                }
            }
        });
    });

    var html = '<div class="form-group col-lg-12 inline-iconsbox"><input type="text" class="form-control" name="ph_numbers_to_send_notification[]"/><i class="fa fa-trash remove" aria-hidden="true"></i>';

    $(".add").click(function(){
        $(".notify").append(html);
    })
    $(document.body).on('click', '.remove' ,function(){
        $(this).parent().remove();
    })
</script>
@endsection
@endsection
