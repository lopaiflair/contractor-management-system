@extends('adminlte::layouts.app')

@section('htmlheader_title',"Add Address")

@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-xs-12">
                @if ($message = Session::get('success'))
                <div>
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div>
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif

                <div class="col-xs-12 box add-padbox">
                    <form method="POST" action="{{url('template/store')}}" id="myform">
                        @csrf
                        <div class="col-lg-6">
                            <?php 
                                $name= '';
                                $description= '';
                                if(!empty($editeData)){
                                    $name= !empty($editeData->name) ? $editeData->name : '';
                                    $description= !empty($editeData->description) ? $editeData->description : '';
                                } ?>
                                <?php if(!empty($editeData)){?>
                                <input type="hidden" name="template_id" value="<?=$editeData->id?>" id="template_id">
                                <?php } ?>
                            <div class="form-group">
                                <label for="name">Template Name <span class="asterisk">*</span></label>
                                <input class="form-control" name="name" type="text" id="name" value="<?=$name?>" placeholder="Template Name"> 
                            </div>

                            <div class="form-group">
                                <label for="name">Description (<span id="counterChar">120</span> characters) <span class="asterisk">*</span></label>
                                <textarea class="form-control" placeholder="Template Description" id="description" name="description" rows="10" maxlength="120"><?=$description?></textarea>
                                <!-- <p>Total characters :<span id="counterChar"></span></p> -->
                            </div>
                       
                            <div class="form-group">
                              <input type="radio" id="active" name="status" value="active" 
                                <?php 
                                    if(!empty($editeData)){
                                        if($editeData->status == "active"){
                                            echo "checked";
                                        }
                                    }
                                     
                                ?>
                                >
                                <label for="active">Active</label>
                                
                                <input type="radio" id="inactive" name="status" value="inactive"
                                <?php 
                                if(!empty($editeData)){
                                    if($editeData->status == "inactive"){ 
                                        echo "checked";
                                    }
                                }
                                ?>
                                >
                                <label for="inactive">In Active</label>
                            </div>
                       
                            <div class="">
                                <input type="submit" value="Submit" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                                <a href="{{url('template')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @section('javascripts')
    <script type="text/javascript">
        $(function() {
            $("#counterChar").html(120 - $('#description').val().length);
            
            jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });
            
            $("#myform").validate({
                rules: {
                    name: {
                      required: true,
                      
                    },
                    description: {
                      required: true,
                      maxlength: 120
                      
                    },
                    status:{
                        required:true,
                    }
                   
                },
                messages: {
                    name: {
                      required: "Please enter template name",
                      
                    },
                    description: {
                      required: "Please enter description",
                      maxlength: "Please enter no more than 120 characters."
                    },
                    status:{
                        required:"Please select Status",
                    }
                   
                }
            });
        });
        $('#description').keyup(function(){
           var str=$('#description').val().length;
           var subtract = 120 - str;
           $('#counterChar').text(subtract);
         });
        /*function limitText(limitField, limitCount) {
            $("#"+limitCount).html(limitField.value.length);
        }*/
    </script>
    @endsection
@endsection
