@extends('adminlte::layouts.app')

@section('htmlheader_title',"Addresses")

@section('main-content')

	<div class="container-fluid spark-screen all_contractor">
		<div class="row">
			<div class="col-xs-12">
				@if ($message = Session::get('success'))
                <div class="success_msg">
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="error_msg">
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<div class="box main-section">
					<div class="box-body table-responsive no-padding">
					    <div class="defalt-btbox-div">
					    	<div class="col-xs-12">
	                            <a href="{{url('template/create')}}" class="add_address_btn" data-placement="top" data-toggle="tooltip" data-original-title="Add Contractor">Add Template</a>
                        	</div>
                       </div>

                        <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="TemplateTable">
		                    <thead>
		                        <tr>
		                        	<th style="display:none;">ID</th>
		                            <th>Name</th>
		                            <th style="width: 600px">Description</th>
		                            <th>Status</th>
		                            <th class="th_disable">Action</th>
		                        </tr>
		                    </thead>
			            </table>
		        	</div>
				</div>
			</div>
		</div>
	</div>
	@section('javascripts')
	<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
	<script type="text/javascript">
		$(function($){
			setTimeout(function() {
		        $(".success_msg").hide('blind', {}, 300);
		        $(".error_msg").hide('blind', {}, 300);

		    }, 5000);
			var dataTable = $('#TemplateTable').DataTable( {
			  	"processing": true,
				"serverSide": true,
				"ajax":{
	                "url": '{{url("template/GetAllTemplateData")}}',
	                "dataType": "json",
	                "type": "POST",
	                "data":{ '_token': "{{csrf_token()}}"}
             	},
	            "method":'post',
			  	"columns": [
			  		{ "data": "id", "name":"id"},
				    { "data": "name", "name":"name"},
				    { "data": "description", "name":"description",class: "desc_header"},
				    { "data": "status",
		                render: function(status,type,row){
		                    //let data = row.data();
		                    if(status == "active"){
		                    	return '<span class="lblsuccess">Active</span>'
		                    }else{
		                    	return '<span class="lbldanger">In Active</span>'
		                    }
		                    
		                }
		            },
				    { "data": "id",
			            "render": function(id, type, row ){
			                  return '<a class="editbtn" data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="template/edit/'+id+'">Edit</a> <a href="{{ url("template/delete") }}/'+id+'" onclick="return confirm('+"'Are you sure you want to delete this record?'"+');" class="deletebtn" data-toggle="tooltip" data-original-title="Delete">Delete</a>';
			            }
			            ,searchable: false, sortable: false
				    },
				],
				"order": [ 0, 'desc' ],
	        	"columnDefs": [
		            {
		                "targets": [ 0 ],
		                "visible": false,
		                "searchable": false,
		            }
	        	]
			});
		});
	</script>
	@endsection
@endsection

