@extends('adminlte::layouts.app')

@section('htmlheader_title',"Add Address")

@section('main-content')
  <div class="container-fluid spark-screen invitation_page">
      <div class="row">
          <div class="col-xs-12">
                @if ($message = Session::get('success'))
                <div class="success_msg">
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="error_msg">
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif

                <div class="col-xs-12 box add-padbox pos-relative">
                    <form method="POST"  id="myform" enctype="multipart/form-data">

                        @csrf
                         <div class="col-lg-12">
                           <div class="form-group">
                            <!-- <label for="selectall">Select All </label>
                            <input type="checkbox" id="selectall" name="selectall" value="true"> -->
                              <div class="form-check">
                    <input type="checkbox" class="form-check-input" value="true" id="exampleCheck1">
                    <label class="form-check-label" for="exampleCheck1">Select All</label>
                  </div>
                           </div>
                       </div>
                       <div class="col-lg-12">
                           <div class="form-group tot_con">
                              <label for="contractors">Total Contactors Selected: <span id="selectedContractor"></span></label>

                           </div>
                       </div>
                        <div class="col-lg-12">
                           <div class="form-group frm_con_type">
                            <label for="contractors">Type of contractor:</label>
                            <div class="type_con">
                              <input type="radio" id="byskill" name="typeof" value="byskill" checked="checked" class="typeselection">
                              <label for="byskill">By skill</label>
                            </div>
                             <div class="type_con">
                              <input type="radio" id="bycontractor" name="typeof" value="bycontractor" class="typeselection">
                              <label for="bycontractor">By contactor</label>
                            </div>
                           <!--  <a href="{{url('contractors/export')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="btn-faith export_button" style="display: none;">Export Contractor</a> -->
                           </div>
                       </div>
                        <div class="col-lg-12">
                           <div class="form-group type_text">
                            <label for="project_name">Project name <span class="asterisk">*</span></label>
                            <input type="text" name="project_name" value="" id="project_name" class="form-control">
                          </div>
                       </div>
                        <div class="col-lg-12" id="skilldiv">
                            <div class="form-group">

                                <label for="skills">Skills<span class="asterisk">*</span> </label>
                                <select name="skills[]" class="form-control" id="skills" multiple="multiple">
                                    @if(!empty($Skills))
                                        @foreach ($Skills as $Skillskey => $Skillsvalue)
                                            <option value="{{ $Skillsvalue['id'] }}">{{ $Skillsvalue['skill'] }}</option>
                                        @endforeach
                                    @endif
                                </select>

                            </div>
                            <!-- <label id="skills-error" class="error" for="skills"></label> -->
                        </div>
                        {{--<div class="col-lg-12" id="contactordiv" style="display: none">
                            <div class="form-group">

                                <label for="contractors">Contractors<span class="asterisk">*</span></label>
                                <select name="contractors[]" class="form-control" id="contractors" multiple="multiple">
                                @if(!empty($Contractors))
                                    @foreach($Contractors as $row)
                                        <option value="{{ $row['id'] }} ">{{ $row['name'] }}</option>
                                    @endforeach
                                @endif
                                </select>
                            </div>
                        </div>--}}
                        <div class="col-lg-12" id="contactordiv" style="display: none">
                            <div class="form-group ui-widget">

                                <label for="contractors">Contractors<span class="asterisk">*</span></label>
                                <textarea id="contractors" name="contractors" class="form-control" rows="5"></textarea>
                            </div>
                        </div>

                       <div class="col-lg-12">
                           <div class="form-group type_text">
                            <!-- <label for="message">Type Text Message(<span id="counterChar">120</span> characters) <span class="asterisk">*</span></label> -->
                            <label for="message">Type Text Message</label>
                            <textarea name="message" rows="5" cols="100" id="message"></textarea>
                           </div>
                       </div>
                       <div class="col-lg-12">
                           <div class="form-group">
                            <label for="document">Upload Document (.pdf, .jpeg, .jpg, .png, .mp4)</label>
                            <input type="file" name="document" class="form-control">

                           </div>
                       </div>

                        <div class="col-lg-12">
                           <div class="form-group">
                            <label for="mobile_number">Phone No</label>
                            <select name="mobile_number" id="mobile_number" class="form-control">
                                <option value="">Please select from number</option>
                                @if (!empty($mobile_number))
                                    @foreach ($mobile_number as $mobile_key => $mobile_number)
                                        <option value="{{ $mobile_number->mobileno }}" {{ ($mobile_number->status == 1) ? "selected" : "" }}>{{ $mobile_number->mobileno }}</option>
                                    @endforeach
                                @endif
                            </select>

                           </div>
                       </div>
                        <div class="col-lg-6">
                          <div class="form-group invitation_btns">
                              <input type="submit" value="Send Message" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                               <a href="{{url('dashboard')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                          </div>


                      </div>

                    </form>
                  <div class="pgre-barbox m-auto progrssbar-pos" id="progressbar_overlay" style="display: none;">
                  <div class="progressbar-circle">
                    <div class="" role="progressbar" id="progress">
                        <span class="progress__label"></span>
                    </div>
                  </div>
                  </div>
                </div>
            </div>
      </div>
  </div>

@endsection
@section('javascripts')
<!-- started -->
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
  <script type="text/javascript" src="{{asset('/plugins/jquery-asPieProgress-master/dist/jquery-asPieProgress.js')}}"></script>
  <script type="text/javascript" src="{{asset('/plugins/toastr-master/toastr.js')}}"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <script type="text/javascript">
      function selectedCount(){
         var radioValue = $("input[name='typeof']:checked").val();
          if(radioValue == 'bycontractor'){
            var countselected = $("#contractors").val().split( /,\s*/ );
            countselected = countselected.filter(item => item);
            $('#selectedContractor').text(countselected.length);
          } else if(radioValue == 'byskill'){
            var countselected = $("#skills :selected").length;
            if(countselected > 0){
                var selectedValues = $('#skills').val();
                   $.ajax({
                       type: "post",
                       url: "{{url('invite/getCountContactor')}}",
                       data: {
                              "skills": selectedValues,
                              "_token" : "{{ csrf_token() }}"
                            },
                       success: function(data)
                       {
                         var countVal = JSON.parse(data);
                         $('#selectedContractor').text(countVal.count);

                       }
                 });
            }else{
                $('#selectedContractor').text(countselected);
            }
          }
      }
      function split( val ) {
        return val.split( /,\s*/ );
      }
      function extractLast( term ) {
        return split( term ).pop();
      }
       $(function() {
            /*$("#contractors").select2({
                minimumInputLength: 1,
                minimumResultsForSearch: Infinity,
                placeholder: "Select Contractor",
                closeOnSelect: false
            });*/
            
            $( "#contractors" )
              .on( "keydown", function( event ) {
                if ( event.keyCode === $.ui.keyCode.TAB &&
                    $( this ).autocomplete( "instance" ).menu.active ) {
                  event.preventDefault();
                }
              }).autocomplete({
                minLength: 0,
                source: function( request, response ) {
                    $.ajax({
                        url: "{{ route('invite.get_contactors') }}",
                        type: 'post',
                        dataType: "json",
                        data: {
                            term: extractLast( request.term ),
                            selected: $("#contractors").val()
                        },
                        success: function( data ) {
                            response( data );
                        }
                    });
                },
                focus: function() {
                  // prevent value inserted on focus
                  return false;
                },
                select: function( event, ui ) {
                  var terms = split( this.value );
                  terms.pop();
                  terms.push( ui.item.label );
                  terms.push( "" );
                  this.value = terms.join( ", " );
                  return false;
                },
                change: function( event, ui ) {
                    selectedCount();
                } 
              });
            
            $('#skills').select2();

            $('.typeselection').click(function(){
                 var radioValue = $("input[name='typeof']:checked").val();
                  if(radioValue == 'bycontractor'){
                     $('#skilldiv').hide();
                     $('#contactordiv').show();
                  }else if(radioValue == 'byskill'){
                     $('#skilldiv').show();
                     $('#contactordiv').hide();
                  }
                  $('#skills option').prop('selected', false);
                  $('#skills').trigger('change');
                  $('#contractors option').prop('selected', false);
                  $('#contractors').trigger('change');
                  $("#exampleCheck1").prop("checked",false);
                  $('#selectedContractor').text("");
            });

            $('#exampleCheck1').click(function(){
              var radioValue = $("input[name='typeof']:checked").val();
              if ($(this).prop("checked") == true){
                  if(radioValue == 'byskill'){
                     $('#skills option').prop('selected', true);
                     $('#skills').trigger('change');
                  } else if(radioValue == 'bycontractor'){
                      @if(!empty($Contractors))
                          $('#contractors').val("{!! implode(', ',array_column($Contractors, 'name')) !!}");
                      @endif
                  }
              } else if($(this).prop("checked") == false){
                  if(radioValue == 'byskill'){
                     $('#skills option').prop('selected', false);
                     $('#skills').trigger('change');
                  } else if(radioValue == 'bycontractor'){
                     $('#contractors').val("");
                  }
              }
              selectedCount();
            });

            $('#contractors').change(function(){
              selectedCount();
            });
            $('#skills').change(function(){
              selectedCount();
            });
            $(document).on("click",".select2-selection__choice__remove",function() {
                selectedCount();
            });

            jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });
            $.validator.addMethod('filesize', function (value, element, arg) {
                var total=0;
                for(var i in element.files) {
                    if(element.files[i].size != undefined){
                        total += element.files[i].size;
                    }
                }
                total = total/1024/1024;
                total = Math.round(total);
                if(total<=arg){
                    return true;
                }else{
                    return false;
                }
            });
            $("#myform").validate({
                rules: {
                    project_name:{
                      required : true,
                    },
                    'skills[]':{
                      required : true,
                    },
                    'contractors[]': {
                        required: true,
                    },
                    message: {
                      required: true,
                      //maxlength: 120
                    },
                    template: {
                        required: true,
                    },
                    document:{
                      // required:true,
                      // extension: "odt|ppt|pdf|doc|docx|pptx|csv|xlsx|txt"
                      filesize:150,
                      extension: "pdf|jpeg|mp4|jpg|png"
                    },
                    mobile_number:{
                      required:true,
                    }
                },
                messages: {
                    project_name:{
                      required : "Please enter project name",
                    },
                    'skills[]':{
                      required : "Please select skill",
                    },
                    'contractors[]': {
                        "required": "Please select contractor"
                    },
                    message: {
                      required: "Please enter message",
                     // maxlength: "Please enter no more than 120 characters."
                   },
                    template: {
                      required: "Please select any template",
                    },
                    document:{
                      // required:"Please select Document",
                      filesize:" file size must be less than 150 MB.",
                      extension: "Please upload pdf,jpeg,jpg,png and mp4."
                    },
                    mobile_number:{
                      required:'please select phone number.',
                    }
                },
                submitHandler: function(form) {
                  $("#progressbar_overlay").show();
                  $('#progress').asPieProgress({
                      namespace: 'pie_progress',
                      easing: 'linear',
                      speed: 5,
                      trackcolor: '#ccc',
                      fillcolor: 'none',
                      barsize: '9',
                      easing: 'ease',
                      barcolor: '#000',
                      min: 0,
                      max: 100,
                      numberCallback: function numberCallback(n) {
                        'use strict';
                         return n;
                      },
                      first: 100
                  });
                  //ajax
                  //{{url('invite/sendmessage')}}
                  var formData = new FormData(form);
                  $.ajax({
                      xhr: function()
                      {
                        var xhr = new window.XMLHttpRequest();
                        //Upload progress
                        xhr.upload.addEventListener("progress", function(evt){
                          if (evt.lengthComputable) {
                            var percentComplete = evt.loaded / evt.total;
                            var percentage = Math.round(percentComplete*100);
                            $(".progress__label").html(percentage+"%");
                            $('#progress').asPieProgress("go", percentage+'%');
                          }
                        }, false);
                        return xhr;
                      },
                      url: "{{url('invite/sendmessage')}}",
                      type:"POST",
                      data: formData,
                      dataType:'json',
                      contentType: false,
                      // cache:false,
                      processData: false,
                      success:function(response){
                        if(response.status){
                            toastr.success(response.message)
                            setTimeout(function(){ location.reload(); }, 3000);
                        } else {
                            toastr.error(response.message)
                        }
                      },
                      error: function (jqXHR, status, err) {
                        toastr.error(jqXHR.responseJSON.message)
                      },
                      complete: function (jqXHR, status) {
                        $("#progressbar_overlay").hide();
                      }
                  });
                }
            });
        });
    </script>
@endsection
