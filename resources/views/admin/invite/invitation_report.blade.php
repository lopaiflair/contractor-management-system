@extends('adminlte::layouts.app')

@section('htmlheader_title',"Addresses")

@section('main-content')

	<div class="container-fluid spark-screen all_contractor">
		<div class="row">
			<div class="col-xs-12">
				@if ($message = Session::get('success'))
                <div class="success_msg">
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="error_msg">
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">

				<!-- Default box -->
				<div class="box main-section">
					<div class="box-body table-responsive no-padding">
					   

                        <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="UserTable">
		                    <thead>
		                        <tr>
		                        	<th>Project Name</th>
		                            <th>Contractor Type</th>
		                            <th>Send By</th>
		                            <th>Total</th>
		                            <th>Created At</th>
		                            <th class="th_disable">Action</th>
		                  	    </tr>
		                    </thead>

		                    

			            </table>
		        	</div><!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
	@section('javascripts')
	<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
	<script type="text/javascript">
		$(function($){
			setTimeout(function() {
		        $(".success_msg").hide('blind', {}, 300);
		        $(".error_msg").hide('blind', {}, 300);

		    }, 5000);
			 $('#UserTable').DataTable({
                pageLength: 10,
                responsive: true,
                "order": [],
                "processing": true,
                "serverSide": true,
                "ajax":{
                     "url": "{{ url('invite/get_datatable_data') }}",
                     "dataType": "json",
                     "type": "POST",
                     "data":{ _token: "{{csrf_token()}}"}
                },
                "columns": [
                    { "data": "project_name" },
                    { "data": "contractor_type" },
                    { "data": "sent_by" },
                    { "data": "total_count" },
                    { "data": "created_at" },
                    { "data": "action" }
                ],   
               
            });
		});
	</script>
	@endsection
@endsection
