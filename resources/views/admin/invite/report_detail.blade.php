@extends('adminlte::layouts.app')

@section('htmlheader_title',"Add Address")

@section('main-content')

  <div class="container-fluid spark-screen invitation_detail_page">
      <div class="row">
       <div class="col-lg-12 box add-padbox">
              <div class="col-lg-12">
                   <label for="contractors">Project Name: </label> <span id="selectedContractor"> <?=$getinvite->project_name?></span>
             </div>

              <div class="col-lg-12">
                    <label for="contractors">Contactor Type: </label> <span id="selectedContractor"> <?=$getinvite->contractor_type?></span>
             </div>

              <div class="col-lg-12">
                    <label for="contractors">Message:  </label> <span> <?=$getinvite->message?></span>
               </div>

             <div class="col-lg-12">
                    <label for="contractors">Total Count:  </label> <span> <?=$getinvite->total_count?></span>
             </div>

             <div class="col-lg-12">
                 <label for="contractors">Sent By : </label><span> <?=$getinvite->sent_by?></span>
                    
             </div>
             <div class="col-lg-12">
                <?php if(!empty($getinvite->skills)){
                    $skills = unserialize($getinvite->skills);
                    $arr = [];
                    if(!empty($skills)){
                      foreach ($skills as $skey => $svalue) {
                          $skilldata = DB::table('skills')->where('id',$svalue)->first();
                          if(!empty($skilldata)){
                              $arr[]     = $skilldata->skill;
                          }
                      }
                    }
                    // pre($arr);
                    // foreach()
                ?>
                  <label for="contractors">Skills : </label><span> <?=implode(',',$arr);?></span>
                <?php } ?>
             </div>

              <div class="col-lg-12">
                    <label for="contractors">Created at : </label><span> <?=$getinvite->created_at?></span>
              </div>
             
              <div class="col-lg-12">
                  <div class="form-group invitation_btns">
                      <a href="{{url('invite/report_invitation')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                  </div>
              </div>
        </div>
      </div>
      <div class="row">
       <div class="col-lg-12 box add-padbox">
              <div class="col-lg-12">
                  <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="UserTable">
                        <thead>
                            <tr>
                                <th>Contractor Name</th>
                                <th>Mobile no</th>
                                <th>Email</th>
                                <th class="th_disable">Message sent</th>
                                <th class="th_disable">Email Sent</th>
                                <th class="th_disable">Sent at</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php if(!empty($getinvite_details)){ 
                                  foreach($getinvite_details as $key => $val){
                                    $contractors = DB::table('contractors')->where('id',$val->contractor_id)->first();
                                    //pre($contractors);
                           ?>
                          <tr>
                            <td>
                                <?php
                                    if(!empty($contractors->name)){
                                        echo $contractors->name;
                                    }else{
                                        echo "";
                                    }
                                ?>
                            </td>
                            <td>
                                <?php
                                    if(!empty($contractors->name)){
                                        if($contractors->mobile_no == 0){
                                          echo "-";
                                        }else{
                                          echo $contractors->mobile_no;
                                        }
                                    }else{
                                        echo "-";
                                    }
                                ?>
                              
                            </td>
                             <td><?=$val->email?></td>
                            <td>
                              <?php 
                                if($val->message_sent == 1){ ?>
                                  <span class="label label-success">Yes</span>
                               <?php }else{ ?>
                                  <span class="label label-danger">No</span>
                               <?php }
                              ?>
                            </td>
                           
                            <td>
                              <?php 
                                if($val->email_sent == 1){ ?>
                                  <span class="label label-success">Yes</span>
                               <?php }else{ ?>
                                  <span class="label label-danger">No</span>
                               <?php }
                              ?>
                            </td>
                            <td><?=$val->created_at?></td>
                          </tr>
                        <?php } }else{ ?>
                           <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="6">No records found..!</td>
                            <td></td>
                            <td></td>
                          </tr>
                       <?php  } ?>
                        </tbody>
                        

                  </table>
              </div>
      </div>
    </div>
  </div>

  @section('javascripts')
  <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>

<script type="text/javascript">
    $(function($){
       $('#UserTable').DataTable();
    });
  </script>
  

  @endsection
@endsection
