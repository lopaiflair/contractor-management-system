@extends('adminlte::layouts.app')

@section('htmlheader_title',"Addresses")

@section('main-content')

	<div class="container-fluid spark-screen skill_page">
		<div class="row">
			<div class="col-xs-12">
				 @if ($message = Session::get('success'))
                <div class="success_msg">
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="error_msg">
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">

				<!-- Default box -->
				<div class="box main-section">
					<div class="box-body table-responsive no-padding">
					
                        <div class="defalt-btbox-div skill_sec">
                             <a href="{{url('skills/create')}}" class="addskillbtn" data-placement="top" data-toggle="tooltip" data-original-title="Add Skill">Add Skill</a>
                       </div>
                   
           
                        <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="UserTable">
		                    <thead>
		                        <tr>
		                        	<th style="display:none;">ID</th>
		                            <th>Skills</th>
		                            <th>Created At</th>
		                            <th>Status</th>
		                           	<th class="th_disable">Action</th>
		                        </tr>
		                    </thead>

		                   

			            </table>

		        	</div><!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
	@section('javascripts')
	<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
	<script type="text/javascript">
		$(function($){
			setTimeout(function() {
		        $(".success_msg").hide('blind', {}, 300);
		        $(".error_msg").hide('blind', {}, 300);

		    }, 5000);
			var dataTable = $('#UserTable').DataTable( {
			  	"processing": true,
				"serverSide": true,
				"ajax":{
	                "url": '{{url("skills/get_datatable_data")}}',
	                "dataType": "json",
	                "type": "POST",
	                "data":{ '_token': "{{csrf_token()}}"}
	             },
	            "method":'post',
			  	"columns": [
			  		{ "data": "id", "name":"id"},
				    { "data": "skill", "name":"skill"},
				    { "data": "created_at", "name":"created_at"},
				    { "data": "status",
		                render: function(status,type,row){
		                    //let data = row.data();
		                    if(status == 1){
		                    	return '<span class="lblsuccess">Active</span>'
		                    }else{
		                    	return '<span class="lbldanger">In Active</span>'
		                    }
		                    
		                }
		            },
				   /* { "data": "skills", "name":"skills"},
				    { "data": "mobile_no", "name":"mobile_no"},
				    { "data": "office", "name":"office"},
				    { "data": "comment", "name":"comment"},
				    { "data": "created_at", "name":"created_at"},*/
				    { "data": "id",
			            "render": function(id, type, row ){
			                  /* return '<a class="btn btn-lg" data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="skills/edit/'+id+'">' + '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>' + '</a><form action="{{ url("skills/delete") }}/'+id+'" method="POST" onsubmit="return confirm(\'Are you sure you want to delete this record?\')">{{ csrf_field() }}{{ method_field("DELETE") }}<button type="submit" data-placement="top" data-toggle="tooltip" data-original-title="Delete"><i class="fa fa-trash-o" aria-hidden="true"></i></button></form>';*/
			                  return '<a class="editbtn" data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="skills/edit/'+id+'">Edit</a> <a href="{{ url("skills/delete") }}/'+id+'" onclick="return confirm('+"'Are you sure you want to delete this record?'"+');" class="deletebtn" data-toggle="tooltip" data-original-title="Delete">Delete</a>';
			            }
			            ,searchable: false, sortable: false
				    },
				],
				"order": [ 0, 'desc' ],
	        	"columnDefs": [
		            {
		                "targets": [ 0 ],
		                "visible": false,
		                "searchable": false
		            }
	        	]
			});
		});
	</script>
	@endsection
@endsection
