@extends('adminlte::layouts.app')

@section('htmlheader_title',"Add Address")

@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-xs-12">
                @if ($message = Session::get('success'))
                <div>
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div>
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif

                <div class="col-xs-12 box add-padbox">
                    <form method="POST" action="{{url('skills/store')}}" id="myform" enctype="multipart/form-data">

                        @csrf
                        <div class="col-lg-6">
                            <?php 
                                
                                $skill= '';
                                
                                if(!empty($editeData)){
                                    $skill= !empty($editeData->skill) ? $editeData->skill : '';
                                  
                                }?>

                            <?php if(!empty($editeData)){?>
                                <input type="hidden" name="skill_id" value="<?=$editeData->id?>" id="skill_id">
                            <?php } ?>
                                
                            <div class="form-group">
                                <label for="skill">Skill <span class="asterisk">*</span></label>
                                <input class="form-control" name="skill" type="text" id="skill" value="<?=$skill?>" placeholder="Skill">
                                
                            </div>
                       
                            <div class="form-group">
                                
                                          <input type="radio" id="active" name="status" value="1" 
                                            <?php 
                                                if(!empty($editeData)){
                                                    if($editeData->status == 1){
                                                        echo "checked";
                                                    }
                                                }
                                                 
                                            ?>
                                            >
                                            <label for="active">Active</label>
                                
                                    <input type="radio" id="inactive" name="status" value="0"
                                    <?php 
                                    if(!empty($editeData)){
                                        if($editeData->status == 0){ 
                                            echo "checked";
                                        }
                                    }
                                    ?>
                                    >
                                    <label for="inactive">In Active</label>
                                
                            </div>
                       
                        <div class="">
                            <input type="submit" value="Submit" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                            <a href="{{url('skills')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @section('javascripts')
    <script type="text/javascript">
        $(function() {
            jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });
            
            $("#myform").validate({
                     rules: {
                        skill: {
                          required: true,
                          
                        },
                        status:{
                            required:true,
                        }
                       
                    },
                    messages: {
                        
                        skill: {
                          required: "Please enter skill",
                          
                        },
                        status:{
                            required:"Please select Status",
                        }
                       
                    }
            });
        });
    </script>
    @endsection
@endsection
