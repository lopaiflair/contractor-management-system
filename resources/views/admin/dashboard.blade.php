@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">

				<!-- Default box -->
				<div class="box">
					<div class="box-header with-border">
						<h3 class="box-title">Home</h3>

						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
								<i class="fa fa-minus"></i></button>
							<button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
								<i class="fa fa-times"></i></button>
						</div>

						<input type="text" id="date" class="date_picker">

						<input type="text" class="daterange_picker">

						<input type="text" class="color_picker">

						<input type="text" class="time_picker">

						<textarea name="ck" id="ckeditor"></textarea>
						<table class="datatable">
							<thead>
								<tr>
									<td>Name</td>
									<td>Phone Number</td>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Name</td>
									<td>Phone Number</td>
								</tr>
								<tr>
									<td>Name</td>
									<td>Phone Number</td>
								</tr>
								<tr>
									<td>Name</td>
									<td>Phone Number</td>
								</tr>
							</tbody>
						</table>

						<canvas id="signature-pad" class="signature-pad" width=400 height=200 style="border:1px solid;"></canvas>
						<input type="button" id="save" value="save">
					</div>
					<div class="box-body">
						{{ trans('adminlte_lang::message.logged') }}. Start creating your amazing application!
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
	@section('javascripts')
	<script src="https://cdn.jsdelivr.net/npm/signature_pad@2.3.2/dist/signature_pad.min.js"></script>
	<script type="text/javascript">
		var canvas = document.querySelector("canvas");

		var signaturePad = new SignaturePad(canvas);
		$("#save").on('click',function(){
			var data = signaturePad.toDataURL('image/png');
			console.log(data);
			window.open(data);
		})

		$(".date_picker").datepicker();

		$(".daterange_picker").daterangepicker();

		$(".color_picker").colorpicker();

		$(".time_picker").timepicker();

		$(document).ready(function () {
			CKEDITOR.replace('ck');
		});

		$(".datatable").DataTable();
	</script>
	@endsection
@endsection
