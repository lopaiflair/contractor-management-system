@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen all_contractor">
		<div class="row">
			<div class="col-xs-12">
				<!-- Default box -->
				<div class="box main-section schedule_date">
					<div class="box-body table-responsive no-padding">


						
						<form method="POST" action="{{url('scheduler/schedule_select_save')}}" class="bookslot" id="myform" enctype="multipart/form-data"> 
			 	 		@csrf
			 	 		<div class="col-lg-12 time_slots">
							
							<div class="slots"><input type="radio" id="slot_1" name="available_slot" value="10:00-10:30">
							<label for="slot_1">10:00 am – 10:30 am</label></div>
							<div class="slots"><input type="radio" id="slot_2" name="available_slot" value="10:45-11:15">
							<label for="slot_2">10:45 am – 11:15 am</label></div>
							<div class="slots"><input type="radio" id="slot_3" name="available_slot" value="11:45-12:15">
							<label for="slot_3">11:45 am – 12:15 pm</label></div>
							<div class="slots"> <input type="radio" id="slot_4" name="available_slot" value="12:30-13:00">
							<label for="slot_4">12:30 pm – 1:00 pm</label></div>
							<div class="slots"><input type="radio" id="slot_5" name="available_slot" value="13:15-13:45">
							<label for="slot_5">1:15 pm – 1:45 pm</label></div>
						</div>
 
						<div class="col-lg-12">
							<label>Select Date</label>
				     		<div id="datepicker" data-date=""></div>
				     	</div>
			        		<input type="hidden" name="schedule_date" value="" id="my_hidden_input">
 							
			        		<div class="row">
			   					<div class="col-lg-12" class="center">
			                        <div class="form-group bottom_btn_group">

			                           <button type="submit" value="Submit" class="btn btn-primary">Submit</button>
			                            <a href="{{url('dashboard')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
			                        </div>
			                    </div>
			                </div>
		   				</form>


					</div>
				</div>
			</div>
		</div>
	</div>
		 
		
 

	@section('javascripts')
	{{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">   --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
    {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>   --}}
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>   --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
 
	<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>


 <script type="text/javascript">
  $('#start_time').timepicker('setTime', new Date(new Date().getTime()+6*3600*1000));

  
 	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var output = (month<10 ? '0' : '') + month + '/' +
    (day<10 ? '0' : '') + day +'/' +
    d.getFullYear() ;
 //
$('#datepicker').data('date', output);
$('#my_hidden_input').val(output);

  $('#datepicker').datepicker({ 
        startDate: new Date(),
        dateFormat: 'dd/mm/yyyy' 
    });
 	 
	$('#datepicker').on('changeDate', function() {
    	$('#my_hidden_input').val(
        	$('#datepicker').datepicker('getFormattedDate')
    	);
    	 
	}); 
	 
 	/*function submit () {

		var date=$('#datepicker').datepicker("getDate");
		var selected_time= $("#selected_time").val();
        $.ajax({
            url:" {{ route('save_date') }} ",    //the page containing php script
            type: "post",    //request type,
            dataType: 'json',
            data: {date: date ,selected_time:selected_time},
            success:function(result){
                console.log(result.abc);
            }
        });
    }  */ 
	$("#myform").validate({
	 
	rules: {
		available_slot: {
			required: true,
		},
		 
	 
	
	},
	messages:{
		available_slot:{
			required: "Please select any one available slot"
		},
		 
	},
	errorPlacement: function (error, element) {
		error.insertAfter(".time_slots");
	}
});
        </script>

	@endsection    
@endsection