@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Log in
@endsection

@section('content')
<body class="hold-transition login-page">
    <div id="app" v-cloak>
        <div class="login-box">
            <div class="login-logo">
                <a href="{{ url('login') }}">
                    <img src="{{ asset('public/img/front_logo.png') }}" alt="Faith Properties" class="login_logo" />
                </a>
            </div><!-- /.login-logo -->

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body">
            <div class="logintxt">Login</div>
        <form action="{{ url('login') }}" method="post" class="login-frm">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="form-group has-feedback">
                <label for="uname"><b>E-Mail Address</b></label>
                <input type="email" class="form-control" placeholder="{{ trans('adminlte_lang::message.email') }}" name="email" value="{{Cookie::get('email')}}" />
              <!--   <span class="glyphicon glyphicon-envelope form-control-feedback"></span> -->
            </div>
            <div class="form-group has-feedback">
                <label for="uname"><b>Password</b></label>
                <input type="password" class="form-control" placeholder="{{ trans('adminlte_lang::message.password') }}" name="password" value="{{Cookie::get('password')}}" />
               <!--  <span class="glyphicon glyphicon-lock form-control-feedback"></span> -->
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember" id="remember" @if(Cookie::has('remember_me')){{"checked"}}@endif> {{ trans('adminlte_lang::message.remember') }}
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-7">
                    <button type="submit" class="btn-faith btn-block btn-flat login_btn">Login</button>
                </div><!-- /.col -->
                <div class="col-xs-5">
                     <div class="col-xs-12 forgot_pass">
                        <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password?
                        </a>
                    </div>
                </div>
            </div>
           <!--  <div class="row">
                <div class="col-xs-12 forgot_pass">
                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Forgot Your Password?
                    </a>
                </div>
            </div> -->
        </form>
    </div>

    </div>
    </div>

    @section('javascripts')
    <script type="text/javascript">
        $(document).ready(function(){
            $("input").iCheck({
                checkboxClass: 'icheckbox_square-yellow',
            });
        })
    </script>
    @endsection
</body>
@endsection
