@extends('adminlte::layouts.auth')

@section('htmlheader_title')
    Password recovery
@endsection

@section('content')

<body class="login-page">
    <div id="app">

        <div class="login-box">
        <div class="login-logo">
            <a href="{{ url('login') }}">
                <img src="{{ asset('public/img/front_logo.png') }}" alt="Faith Properties" class="login_logo" />
            </a>
        </div><!-- /.login-logo -->

        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> {{ trans('adminlte_lang::message.someproblems') }}<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="login-box-body email_frm reset_pwd">
            <p class="login-box-msg">Reset Password</p>

            <email-reset-password-form></email-reset-password-form>

            <a href="{{ url('login') }}" class="btn-faith btn-block btn-flat login_btn">Log in</a>
        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->
    </div>
</body>

@endsection
