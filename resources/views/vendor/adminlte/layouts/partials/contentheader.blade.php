<!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        @yield('contentheader_title', $contentheader_title)
    </h1>

    <ol class="breadcrumb">
        @if(isset($breadcrum))
	    <?php //print_r($breadcrum); ?>
	    <ol class="breadcrumb">
	        <?php $brs = count($breadcrum);
$new_brs = $brs - 1;
for ($i = 0; $i < $brs; $i++) {?>
	        <li <?php if ($i == 0) {?>class="active"<?php }?>>
	            <?php if ($i != $new_brs) {?>
	            <a href="<?php echo $breadcrum[$i]['url'] ?>">
	                <?php if ($i == 0) {?><i class="fa fa-home"></i> <?php }?>
	                <?php echo $breadcrum[$i]['name'] ?>
	            </a>
	            <?php } else {?>
	            <?php if ($i == 0) {?><i class="fa fa-home"></i> <?php }?>
	                <?php echo $breadcrum[$i]['name'] ?>
	            <?php }?>
	        </li>
	      <?php }?>
	    </ol>
	    @endif
    </ol>

    <!-- <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> {{ trans('adminlte_lang::message.level') }}</a></li>
        <li class="active">{{ trans('adminlte_lang::message.here') }}</li>
    </ol> -->
</section>
