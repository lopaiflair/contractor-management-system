<!-- Main Header -->
<header class="main-header">



    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Logo -->
        <a href="{{ url('/dashboard') }}">
                    <img src="{{ asset('public/img/front_logo.png') }}" alt="Faith Properties" class="dashboard_logo" />
        </a>
        <!-- <a href="{{ url('/dashboard') }}" class="logo">
         
            <span class="logo-mini"><b>F</b>L</span>
           
            <span class="logo-lg"><b>Contractor </b>Management System</span>
           
        </a> -->
        <!-- Sidebar toggle button-->
<!--
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
-->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

              @if(isset(auth()->guard('admins')->user()->role) && auth()->guard('admins')->user()->role == "Admin")
                <li class="{!! Request::is('dashboard') ? 'active' : '' !!}">
                    <a href="{{ url('dashboard') }}"> <span>Home</span></a>
                </li>
                @endif
                 @if(isset(auth()->guard('admins')->user()->role) && auth()->guard('admins')->user()->role == "Admin")
                <li class="{!! Request::is('contractors') ? 'active' : '' !!}">
                    <a href="{{ url('contractors') }}"> <span>Add Contractor</span></a>
                </li>
                @endif
 
                <?php $routeArray = app('request')->route()->getAction();
                      $controllerAction = class_basename($routeArray['controller']);
                      list($controller, $action) = explode('@', $controllerAction);
                      $invite_controller = $controller;
                      $invite_method     = $action;
                      $inv_con_class = '';
                      $inv_report_class = '';
                      if($invite_controller == 'InvitationController' && $invite_method == 'index'){
                        $inv_con_class = 'active';
                      }
                      if($invite_controller == 'InvitationController' && $invite_method == 'report_invitation'){
                        $inv_report_class = 'active';
                      }
                      if($invite_controller == 'InvitationController' && $invite_method == 'show_details'){
                        $inv_report_class = 'active';
                      }
                ?>
                @if(isset(auth()->guard('admins')->user()->role) && auth()->guard('admins')->user()->role == "Admin")
                <li class="dropdown {!! Request::is('invite') ? 'active' : '' !!}">
                   <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Invite <span class="caret"></span></a>
                    <ul class="dropdown-menu" role="menu">
                       <li class="<?=$inv_con_class?>"><a href="{{ url('invite') }}">Invite Contractors</a></li>
                       <li class="<?=$inv_report_class?>"><a href="{{ url('invite/report_invitation') }}">Invitation Report</a></li>
                    </ul>
                </li>
                @endif
                @if(isset(auth()->guard('admins')->user()->role) && auth()->guard('admins')->user()->role == "Admin")
                <li class="{!! Request::is('skills') ? 'active' : '' !!}">
                    <a href="{{ url('skills') }}"> <span>Skills</span></a>
                </li>
                @endif
                @if(isset(auth()->guard('admins')->user()->role) && auth()->guard('admins')->user()->role == "Admin")
                <li class="{!! Request::is('setting') ? 'active' : '' !!}">
                    <a href="{{ url('setting') }}"> <span>Setting</span></a>
                </li>
                @endif
                @if(isset(auth()->guard('admins')->user()->role) && auth()->guard('admins')->user()->role == "Admin")
                <li class="{!! Request::is('template') ? 'active' : '' !!}">
                    <a href="{{ url('template') }}"> <span>Template</span></a>
                </li>
                @endif
              <!--   <li class="{!! Request::is('admin/addresses*') ? 'active' : '' !!}">
                    <a href="{{ url('admin/addresses') }}"><i class="fa fa-address-card" aria-hidden="true"></i> <span>Addresses</span></a>
                </li> -->

                @if (Auth::guard('admins')->guest())
                <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                <!-- User Account Menu -->
                <li class="dropdown user user-menu" id="user_menu" style="max-width: 280px;white-space: nowrap;">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" style="max-width: 280px;white-space: nowrap;overflow: hidden;overflow-text: ellipsis">
                        <!-- The user image in the navbar-->
                        @if(auth()->guard('admins')->user()->profile_picture)
                        <img src="{{ asset('storage/profile_image/'.auth()->guard('admins')->user()->profile_picture)}}" class="user-image" alt="User Image" />
                        @endif
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::guard('admins')->user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            @if(auth()->guard('admins')->user()->profile_picture)
                            <img src="{{ asset('storage/profile_image/'.auth()->guard('admins')->user()->profile_picture)}}" class="img-circle" alt="User Image" />
                            @endif
                            <p>
                                <span data-toggle="tooltip" title="{{ Auth::guard('admins')->user()->name }}">{{ Auth::guard('admins')->user()->name }}</span>

                            </p>
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('/profile') }}" class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    {{ trans('adminlte_lang::message.signout') }}
                                </a>

                                <form id="logout-form" action="{{ url('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                    <input type="submit" value="logout" style="display: none;">
                                </form>

                            </div>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </div>
         <i class="fa fa-bars"></i>
    </nav>
</header>
