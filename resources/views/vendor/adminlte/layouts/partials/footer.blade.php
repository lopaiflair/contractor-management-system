<!-- Main Footer -->
<footer class="main-footer">
    <!-- Default to the left -->
    <strong>Copyright &copy; {{date('Y')}} <a href="{{ url('/admin/profile') }}" class="footer_text">Contractor Management System.</a></strong>
    <span class="footer_span_txt">Designed, Built, and Hosted by mobile.earth</span>
</footer>
