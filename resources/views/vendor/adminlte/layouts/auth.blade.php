<!DOCTYPE html>
<html>

@include('adminlte::layouts.partials.htmlheader')

@yield('content')

@section('scripts')
	@include('adminlte::layouts.partials.scripts_auth')
    @yield('javascripts')
@show

</html>
