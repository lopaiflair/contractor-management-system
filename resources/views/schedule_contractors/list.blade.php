@extends('adminlte::layouts.app')

@section('htmlheader_title',"Addresses")

@section('main-content')

	<div class="container-fluid spark-screen all_contractor">
		<div class="row">
			<div class="col-xs-12" id="msg">
				@if ($message = Session::get('success'))
                <div class="success_msg">
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div class="error_msg">
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12">

				<!-- Default box -->
				<div class="box main-section">
					<div class="box-body table-responsive no-padding">
					    <div class="defalt-btbox-div">

					    	<div class="col-xs-6">
	                            <a href="{{url('bookslot')}}" class="add_address_btn" data-placement="top" data-toggle="tooltip" data-original-title="Add Contractor">Add Schedule Contractor</a>
	                            <button type="button" id="hire_contractor" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">Hire Contractors</button> 
                        	</div>
					    	
					    	 
                             

                       </div>

                        <table class="table table-striped table-bordered" cellspacing="0" width="100%" id="UserTable">
		                    <thead>
		                        <tr>

		                        	<th style="display:none;">ID</th>
		                        	<th></th>
		                        	<th>Schedule Date</th>
		                        	<th>Timeslot</th>
		                            <th>Name</th>
		                            <th>Email</th>
		                            <th>What address you are coming too</th>
		                            <th>Mobile Number</th>
		                            <th>Skill set</th>
		                            <th>Send text message to</th>
		                            <th>Notes</th>
		                            <th>Reminder</th>
		                            <th class="th_disable">Action</th>
		                        </tr>
		                    </thead>

		                    

			            </table>
		        	</div><!-- /.box-body -->
				</div>
				<!-- /.box -->

			</div>
		</div>
	</div>
<!-- start midel code -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel"><b>Hire contractor</b></h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-lg-12">
                <div class="form-group">
                    <label for="Reminder message">Reminder message </label>
                    <textarea class="form-control" id="reminder_message" name="reminder_message" rows="4" cols="20">
                    </textarea>
                    
                </div>
            </div>
      	 	<div class="col-lg-12">
                <div class="form-group">
                    <label for="name">Enable reminder notification </label>
        				<input class= "check-box" type="checkbox" id="reminder_notification" name="reminder_notification" value="">
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label for="name">Interval </label>
        				<select class="form-control interval" disabled="true" name="interval" id="interval" aria-label="interval">
							<option value="daily">Daily</option>
							<option value="hourly">Hourly</option>							
						</select>
                </div>
            </div>
        </div>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" id="tag-form-submit" class="btn btn-primary">Submit</button>
      </div>
    </div>
  </div>
</div>
<!-- end model code -->
@endsection
	@section('javascripts')
<!-- for datamodel dropdown -->
 
<link rel="stylesheet" type="text/css" href="https://unpkg.com/toastmaker/dist/toastmaker.min.css">

<script type="text/javascript" src="https://unpkg.com/toastmaker/dist/toastmaker.min.js"></script>
<script>
$(function() {

  
var sThisVal='';
var array_val = [];
$('#tag-form-submit').on('click', function(e) {
 
	var reminder_message = document.getElementById("reminder_message").value; 
 	if ($('#reminder_notification').is(":checked")) {
           reminder_notification = "on";
        }
        else{
            reminder_notification = "off";
        }
 		var interval = document.getElementById("interval").value;   // Get the element with id="demo"
 /*    e.preventDefault();
*/    	$('.hire_contractor:checkbox:checked').each(function () {
     
	       sThisVal = (this.checked ? $(this).data('id') : "");
	       array_val.push(sThisVal);
		});
		$.ajax({
            url:" {{ url('bookslot/store_model_value') }} ",    //the page containing php script
            type: "post",    //request type,
           dataType: 'json',
            data: {chkbox: array_val ,reminder_message:reminder_message,reminder_notification:reminder_notification, interval:interval},
            success:function(result){
				$("#msg").load(" #msg");
                  
            }
        }); 
	
		$('#UserTable').DataTable().ajax.reload();
 		$("#exampleModal").removeClass("in");
	  	$(".modal-backdrop").remove();
  		$('body').removeClass('modal-open');
	  	$('body').css('padding-right', '');
	  	$("#exampleModal").hide(); 

   return false; 
});

 
});
$(document).ready(function () {
            $(".select").attr("disabled",true);
                 $("input[type='checkbox']").on("change", function () {
                    if ($(this).prop("checked")) {
                    	$('select#interval').removeAttr('disabled');

                         
                     }else{
                     	$('select#interval').attr('disabled',true);
                      }
                });
        });
$('#hire_contractor').on('click', function () {
	var checked = $('.hire_contractor:checked').length;
	if(checked == 0){
		toastr.error("Please select any checkbox");
 		return false;
	}
});
</script>
<!-- end -->
	<link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
	<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.5/jquery-ui.min.js'></script>
	<script type="text/javascript">
		$(function($){
			setTimeout(function() {
		        $(".success_msg").hide('blind', {}, 300);
		        $(".error_msg").hide('blind', {}, 300);

		    }, 5000);
			var dataTable = $('#UserTable').DataTable( {

 			  	"processing": true,
				"serverSide": true,
				 "aaSorting": [[0, 'desc']],
				"ajax":{
	                "url": '{{url("bookslot/get_datatable_data")}}',
	                "dataType": "json",
	                "type": "POST",
	                "data":{ '_token': "{{csrf_token()}}"}
	             },
	            "method":'post',
			  	"columns": [
			  		
			  		{ "data": "id", "name":"id"},
					{ "data": "id",
			             render: function ( data, type, full, meta ) {
		                  	if(full['hired'] == 'no'){ 
			                   		return '<input type="checkbox" class="hire_contractor" id="hire_contractor" data-id="' + full['id'] + '" name="hire_contractor" value="">';
		               		}else{
		               			return '<input style="display:none;" type="checkbox" data-id="' + full['id'] + '" id="hire_contractor" name="hire_contractor" value="">';
		               		}
			            }
			            ,searchable: false, sortable: false
				    },
				    { "data": "schedule_date", "name":"schedule_date"},
				    { "data": "timeslot", "name":"timeslot"},
				    { "data": "name", "name":"name"},
				    { "data": "email", "name":"email"},
				    { "data": "what_address_you_are_coming_to", "name":"what_address_you_are_coming_to"},
				    { "data": "mobile_number", "name":"mobile_number"},
				    { "data": "skillname", "name":"skillname"},
				    { "data": "sent_text_message_to", "name":"sent_text_message_to"},
				    { "data": "notes", "name":"notes"},
				    {
		             "sortable": true,
		             "className":'centerize',
		             render: function ( data, type, full, meta ) {
		                 if(full['reminder'] == 'on'){
		                
		                    return '<input class="togglebtn" value = "on" id="toggle"  data-id="' + full['id'] + '" type="checkbox" name="reminder"  checked data-toggle="toggle" data-on="On" data-off="Off" data-size="lg" data-onstyle="success" data-offstyle="danger">';
		                }else{
		                    return '<input class="togglebtn" value = "on" id="toggle"  data-id="' + full['id'] + '" type="checkbox" name="reminder"   data-toggle="toggle" data-on="On" data-off="Off" data-onstyle="success" data-size="lg" data-offstyle="danger">';

		                }

		             }
		            },
				    { "data": "id",
			            "render": function(id, type, row ){
			                  
			                   return '<a class="editbtn" data-placement="top" data-toggle="tooltip" data-original-title="Edit" href="{{url("bookslot/edit")}}/'+id+'"  >Edit</a> ';
			            }
			            ,searchable: false, sortable: true
				    },
				],
				 
	        	"columnDefs": [
		            {
		                "targets": [ 0 ],
		                "visible": false,
		                "searchable": false,
		            }
	        	],
	        	"fnDrawCallback": function() {
		          $('.togglebtn').bootstrapToggle();
		        },
			});
		});

		 

		$('#UserTable').on('search.dt', function() {
		var value = $('.dataTables_filter input').val();
		}); 
	 
    
   $(function() {
  
  $('body').delegate('.centerize', 'click', function() {
     var $t = $(this);
    var ID = $t.find("input[data-id]").data('id');
    if($t.children('div.off').length > 0) {
        var reminder="on";
    }else{
        var reminder="off";
    }
    
    var csrf = "{{ csrf_token() }}";
    $.ajax({
        url: '{{ url("bookslot/changestatus")}}',
        type: "POST",
        data: { 'id': ID, '_token': csrf,'reminder':reminder },
        success: function (response) {
        	$("#msg").load(" #msg");
          }
    });
     
     
     
     
  });
});
</script>
	@endsection
