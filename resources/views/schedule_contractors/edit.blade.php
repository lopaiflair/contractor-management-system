@extends('adminlte::layouts.app')

@section('htmlheader_title',"Add Address")

@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-xs-12">
                @if ($message = Session::get('success'))
                <div>
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div>
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif

                <div class="col-xs-12 box add-padbox edit_box">
                    <form method="POST" action="{{url('bookslot/edit_store')}}" id="myform" enctype="multipart/form-data">

                        @csrf
                        <div class="col-lg-12">
                            <div class="col-lg-3">
                                <a href="{{ url('/dashboard') }}">
                                    <img src="{{ asset('public/img/favicon.png') }}" width="50" height="50" alt="Faith Properties" />
                                </a>
                                <p>Faith properties, We turn houses into homes</p>
                                <p class="meeting_time"><b><?php echo session('schedule_contractor_total_mins', 'default'); ?> Minutes Meetings</b></p>
                                <p><i class="fa fa-clock-o" aria-hidden="true"></i><b><?php echo session('schedule_contractor_total_mins', 'default'); ?> min</b></p>
                                <p style="color:green;"><i class="fa fa-calendar" aria-hidden="true"></i><b><?php echo session('schedule_contractor_fulldate', 'default'); ?></b></p>
                                <p><i class="fa fa-globe" aria-hidden="true"></i><b>Central Time - US & Canada</b></p>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="notes">Notes </label>

                                        <textarea class="form-control" id="notes" name="notes" rows="4" cols="20">{{ $data->notes }}</textarea>

                                    </div>
                                </div>


                            </div>
                            <div class="col-lg-9">
                                 <input  class="form-control" name="id" type="hidden" id="id" value="{{ $data->id }}">

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="name">Name *</label>
                                        <input class="form-control" disabled="true" name="name" type="text" id="name" value="{{ $data->name }}" placeholder="Name">

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="email">Email *</label>
                                        <input class="form-control" disabled name="email" type="text" id="email" value="{{ $data->email }}" placeholder="Email">

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="what_address_you_are_coming_to">What address you are coming too *</label>
                                        <textarea class="form-control" id="what_address_you_are_coming_to" disabled name="what_address_you_are_coming_to" rows="4" cols="20">{{ $data->what_address_you_are_coming_to }}</textarea>


                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="mobile_number">Mobile Number *</label>
                                        <input class="form-control" disabled name="mobile_number" type="text" id="mobile_number" value="{{ $data->mobile_number }}" placeholder="Mobile Number">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <?php
$skillArr = [];
if (!empty($data)) {
	$skillArr = explode(',', $data->skill_set);
}

?>
                                         <label for="skill_set">Skill set *</label>
                                        <select name="skill_set[]" disabled class="form-control" id="skills" multiple="multiple">
                                            <!-- <option value="">Please select skill</option> -->
                                            <?php

if (!empty($skills)) {
	foreach ($skills as $key => $value) {
		?>
                                                        <option value="<?=$value['id']?>"
                                                            <?php if (!empty($skillArr)) {
			if (in_array($value['id'], $skillArr)) {
				echo "selected";
			}

		}?>
                                                        ><?=$value['skill']?></option>
                                            <?php }
}
?>
                                        </select>

                                    </div>
                                </div>



                                 <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="sent_text_message_to">Send text message to *</label>
                                        <input class="form-control" disabled name="sent_text_message_to" type="text" id="sent_text_message_to" value="{{ $data->sent_text_message_to }}" placeholder="Send text message to ">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="reminder_message">Reminder message (30 minutes before message)</label>

                                        <textarea id="schedule_message" class="form-control" name="schedule_message" rows="4" cols="20">{{ $data->schedule_message }}</textarea>

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    You will opt in to text message for this event. Message and data rates may apply. Reply STOP to opt out.
                                </div>

                            </div>

                        </div>
                        <div class="col-lg-12">
                            <div class="form-group btn-right">
                                <input type="submit" value="Submit" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                                <a href="{{url('bookslot/schedule_contractors_list')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @section('javascripts')
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script type="text/javascript">

        $(function() {
             jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });

            $('#skills').select2();
            jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });



            /*$("#myform").validate({

                rules: {

                    notes: {
                        required: true,
                    },
                    schedule_message: {
                        required: true,
                    },
                },
                messages: {

                    notes:{
                        "required": "Please enter notes.",
                    },
                    schedule_message:{
                        "required": "Please enter schedule message.",
                    },
                }
            });*/


        });
    </script>
    @endsection
@endsection
