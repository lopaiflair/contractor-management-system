@extends('adminlte::layouts.app')

@section('htmlheader_title',"Add Address")

@section('main-content')

    <div class="container-fluid spark-screen">
        <div class="row">
            <div class="col-xs-12">
                @if ($message = Session::get('success'))
                <div>
                    <p class="alert alert-success">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif
                @if ($message = Session::get('error'))
                <div>
                    <p class="alert alert-danger">{{ $message }}<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button></p>
                </div>
                @endif

                <div class="col-xs-12 box add-padbox edit_box">
                    <form method="POST" action="{{url('bookslot/store')}}" id="myform" enctype="multipart/form-data">

                        @csrf
                        <div class="col-lg-12">
                            <div class="col-lg-3">
                                <a href="{{ url('/dashboard') }}">
                                    <img src="{{ asset('public/img/favicon.png') }}" width="50" height="50" alt="Faith Properties" />
                                </a>
                                <p>Faith properties, We turn houses into homes</p>
                                <p class="meeting_time"><b><?php echo session('schedule_contractor_total_mins', 'default'); ?> Minutes Meetings</b></p>
                                <p><i class="fa fa-clock-o" aria-hidden="true"></i><b><?php echo session('schedule_contractor_total_mins', 'default'); ?> min</b></p>
                                <p style="color:green;"><i class="fa fa-calendar" aria-hidden="true"></i><b><?php echo session('schedule_contractor_fulldate', 'default'); ?></b></p>
                                <p><i class="fa fa-globe" aria-hidden="true"></i><b>Central Time - US & Canada</b></p>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="notes">Notes</label>

                                        <textarea class="form-control" id="notes" name="notes" rows="4" cols="20">

                                        </textarea>

                                    </div>
                                </div>

                            </div>
                            <div class="col-lg-9">


                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="name">Name *</label>
                                        <input class="form-control" name="name" type="text" id="name" value="" placeholder="Name">

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="email">Email *</label>
                                        <input class="form-control" name="email" type="text" id="email" value="" placeholder="Email">

                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="what_address_you_are_coming_to">What address you are coming too *</label>
                                        <textarea class="form-control" id="what_address_you_are_coming_to" name="what_address_you_are_coming_to" rows="4" cols="20"></textarea>
                                    </div>
                                </div>

                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="mobile_number">Mobile Number *</label>
                                        <input class="form-control" name="mobile_number" type="text" id="mobile_number" value="" placeholder="Mobile Number">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">

                                         <label for="skill_set">Skill set *</label>
                                        <select name="skill_set[]" class="form-control" id="skills" multiple="multiple">
                                            <!-- <option value="">Please select skill</option> -->
                                            <?php

if (!empty($skills)) {
	foreach ($skills as $key => $value) {?>
                                                        <option value="<?=$value['id']?>"

                                                        ><?=$value['skill']?></option>
                                            <?php }
}
?>
                                        </select>

                                    </div>
                                </div>
                                <!--  <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="skill_set">Skill set</label>
                                        <input class="form-control" name="skill_set" type="text" id="skill_set" value="" placeholder="Skill set">

                                    </div>
                                </div> -->

                                 <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="sent_text_message_to">Send text message to *</label>
                                        <input class="form-control" name="sent_text_message_to" type="text" id="sent_text_message_to" value="" placeholder="Send text message to ">

                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    You will opt in to text message for this event. Message and data rates may apply. Reply STOP to opt out.
                                </div>

                            </div>

                        </div>
                        <div class="col-lg-12">
                            <div class="form-group btn-right">
                                <input type="submit" value="Submit" class="btn-faith" data-toggle="tooltip" data-original-title="Submit">
                                <a href="{{url('bookslot')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @section('javascripts')
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script> -->
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    <script type="text/javascript">
        $(function() {
             jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });

        $('#skills').select2();
            jQuery.validator.setDefaults({
                debug: false,
                success: "valid"
            });



            jQuery.validator.addMethod("maxLen", function (value, element, param) {
                //console.log('element= ' + $(element).attr('name') + ' param= ' + param )
                if ($(element).val().length > param) {
                    return false;
                } else {
                    return true;
                }
            }, "Please enter no more than 10 digits.");

            $("#myform").validate({

                rules: {
                    name: {
                        required: true,
                    },
                    email: {
                        required:true,
                        email:true,
                        // checkExists:true
                         remote: {
                            url: "{{url('bookslot/checkemail')}}",
                            type: 'post',
                            data: {
                              "_token" : "<?php echo csrf_token(); ?>",
                              "c_email": $('#email').val(),
                              //'contractor_id' :$('#c_id').val()
                            }
                        },
                    },
                    what_address_you_are_coming_to:{
                        required: true,
                    },
                    mobile_number: {
                        required:true,
                        digits: true,
                     },
                    skill_set: {
                         required: true,
                    },
                    sent_text_message_to: {
                        required:true,


                    },

                },
                messages: {
                    name:{
                        "required": "Please enter name.",
                    },
                    email: {
                        "required": "Please enter email.",
                        "email" : "Please enter a valid email address.",
                        "remote": "This email is already exist."

                    },
                    what_address_you_are_coming_to: {
                        "required": "Please enter What address you are coming too.",

                    },
                    mobile_number: {
                        "required": "Please enter mobile number.",
                        "digits": "Please enter digits only.",
                    },
                    skill_set: {
                        "required": "Please enter Skill set.",
                    },
                    sent_text_message_to: {
                        "required": "Please enter Send text message to.",
                        "digits": "Please enter digits only.",
                    },

                }
            });


        });
    </script>
    @endsection
@endsection
