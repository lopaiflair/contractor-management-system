@extends('adminlte::layouts.app')

@section('htmlheader_title')
	{{ trans('adminlte_lang::message.home') }}
@endsection


@section('main-content')
	<div class="container-fluid spark-screen all_contractor">
		<div class="row">
			<div class="col-xs-12">
				<!-- Default box -->
				<div class="box main-section schedule_date">
					<div class="box-body table-responsive no-padding">


						<div class="col-lg-12">
							<label>Select Date</label>
				     		<div id="datepicker" data-date=""></div>
				     	</div>
						<form method="POST" action="{{url('bookslot/save_date')}}" class="bookslot" id="myform" enctype="multipart/form-data"> 
			 	 		@csrf
			        		<input type="hidden" name="schedule_date" value="" id="my_hidden_input">
 							<div class="row">
								<div class="col-lg-12">
				                    <div class="form-group">
										<div class="md-form time_picker">
											<label>Select Timeslot</label>
											<div class="row inner_error" >
												<div class="col-lg-5">
													<div class="md-form mx-5 my-5">
														<input type="time" id="start_time" name="start_time" class="form-control timeslot" value="">
 													  </div>
												</div>
												<div class="col-lg-2">
													<label>to</label>
												</div>
												<div class="col-lg-5">
													<div class="md-form mx-5 my-5">
														<input type="time" id="end_time" name="end_time" class="form-control timeslot" value="">
 													  </div>
 											  	</div>
											</div>
 										</div>
										 
									</div>
								</div>
							</div>
			        		<div class="row">
			   					<div class="col-lg-12" class="center">
			                        <div class="form-group bottom_btn_group">

			                           <button type="submit" value="Submit" class="btn btn-primary">Submit</button>
			                            <a href="{{url('dashboard')}}" data-placement="top" data-toggle="tooltip" data-original-title="Back" class="add_address_btn btn-faith back_btn">Back</a>
			                        </div>
			                    </div>
			                </div>
		   				</form>


					</div>
				</div>
			</div>
		</div>
	</div>
		 
		
 

	@section('javascripts')
	{{-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">   --}}
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
    {{-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>   --}}
    {{-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>   --}}
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
 
	<script src="//jonthornton.github.io/jquery-timepicker/jquery.timepicker.js"></script>


 <script type="text/javascript">
  $('#start_time').timepicker('setTime', new Date(new Date().getTime()+6*3600*1000));

  
 	var d = new Date();
	var month = d.getMonth()+1;
	var day = d.getDate();
	var output = (month<10 ? '0' : '') + month + '/' +
    (day<10 ? '0' : '') + day +'/' +
    d.getFullYear() ;
 //
$('#datepicker').data('date', output);
$('#my_hidden_input').val(output);

  $('#datepicker').datepicker({ 
        startDate: new Date(),
        dateFormat: 'dd/mm/yyyy' 
    });
 	 
	$('#datepicker').on('changeDate', function() {
    	$('#my_hidden_input').val(
        	$('#datepicker').datepicker('getFormattedDate')
    	);
    	 
	}); 
	 
 	/*function submit () {

		var date=$('#datepicker').datepicker("getDate");
		var selected_time= $("#selected_time").val();
        $.ajax({
            url:" {{ route('save_date') }} ",    //the page containing php script
            type: "post",    //request type,
            dataType: 'json',
            data: {date: date ,selected_time:selected_time},
            success:function(result){
                console.log(result.abc);
            }
        });
    }  */ 
	$("#myform").validate({
	groups: {
        inputGroup: "start_time end_time"
    },
	rules: {
		start_time: {
			require_from_group: [2, $(".timeslot")]
		},
		end_time: {
			require_from_group: [2, $(".timeslot")]
		},
	 
	
	},
	messages:{
		start_time:{
			require_from_group: "Please select start time and nd time"
		},
		end_time:{
			require_from_group: "Please select start time and end time"
		},

	},
	errorPlacement: function (error, element) {
		error.insertAfter(".inner_error");
	}
});
        </script>

	@endsection    
@endsection