<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
//Route::redirect('/', 'adminlogin');
//Route::redirect('/login', 'admin/login');
//Route::redirect('/register', 'admin/login');
//Route::redirect('/home', 'admin/login');

Route::redirect('/', 'login');
// Route::redirect('/login', 'admin/login');
Route::redirect('/register', 'admin/login');
Route::redirect('/home', 'admin/login');


/*Route::get('send_test_sms', function(){
    \Telnyx\Telnyx::setApiKey('KEY01734F82F8A888910BF268CD1D21D00B_NM5699P4GKPD8lAeqAXdJX');
    $info = \Telnyx\Message::Create([
        'from' => '+18162083113',
        'to' => "+917874220199",
        'text' => "Testing message",
        'webhook_url' => url('success_webook.php'),
        'webhook_failover_url' => url('fail_over_webhook.php'),
    ]);
    dd($info);
});*/
Route::any('webhook/success', 'Admin\InvitationController@smsSuccessWebhook');
Route::any('webhook/fail_over', 'Admin\InvitationController@smsFailOverWebhook');

/*Admin Routes*/

Route::get('admin', 'Admin\LoginController@showLoginForm')->name('admin.login');
// Route::get('admin/login', 'Admin\LoginController@showLoginForm')->name('admin.login');
Route::get('login', 'Admin\LoginController@showLoginForm')->name('admin.login');
// Route::post('admin/login', 'Admin\LoginController@login');
Route::post('login', 'Admin\LoginController@login');
// Route::post('admin/logout', 'Admin\LoginController@logout')->name('admin.logout');
Route::post('logout', 'Admin\LoginController@logout')->name('admin.logout');

Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');

/*Route::post('admin/password/reset', 'Auth\ResetPasswordController@reset')->name('admin.password.update');
Route::get('admin/password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
*/
Route::post('password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');

Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

// Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => ['auth:web,admins']], function () {
Route::group(['namespace' => 'Admin', 'middleware' => ['auth:web,admins']], function () {

    Route::get('dashboard','DashboardController@index')->name('dashboard');
    Route::get('profile', 'LoginController@show_profile');
    Route::post('profile', 'LoginController@update_profile');

    Route::get('addresses/import', 'AddressController@showImportform')->name('addresses.import');
    Route::post('addresses/import', 'AddressController@importCsv')->name('addresses.import');
    Route::post('addresses/get_datatable_data', 'AddressController@getData')->name('addresses.get_datatable_data');
    Route::resource('addresses', 'AddressController');

    /*Only Admin Can Access*/
    Route::resource('settings', 'SettingController')->middleware('admin');
    Route::get('login_history', 'LoginHistoryController@index')->name('login_history.index')->middleware('admin');
    Route::post('login_history/get_datatable_data', 'LoginHistoryController@getData')->name('login_history.getData');
    /* /Only Admin Can Access*/

    //Contractor URLs
    Route::any('contractors','ContractorController@index');
    Route::any('contractors/create','ContractorController@create');
    Route::any('contractors/store','ContractorController@store');
    Route::any('contractors/get_datatable_data','ContractorController@get_datatable_data');
    Route::any('contractors/edit/{id}','ContractorController@edit');
    Route::any('contractors/delete/{id}','ContractorController@destroy');
    Route::any('contractors/checkemail','ContractorController@checkemail');
    Route::any('contractors/checkmobile','ContractorController@checkmobile');
    Route::any('contractors/import','ContractorController@import');
    Route::any('contractors/importdata','ContractorController@importdata');
    Route::any('contractors/addnewskill','ContractorController@addnewskill');
    Route::any('contractors/addquickskill','ContractorController@addquickskill');
    Route::any('contractors/export','ContractorController@exportContractor');
    Route::any('contractors/demoexport','ContractorController@demoExport');
    
    

    //Skills URLs
    Route::any('skills','SkillController@index');
    // Route::any('allskills','SkillController@index');
    Route::any('skills/create','SkillController@create');
    Route::any('skills/store','SkillController@store');
    Route::any('skills/get_datatable_data','SkillController@get_datatable_data');
    Route::any('skills/edit/{id}','SkillController@edit');
    Route::any('skills/delete/{id}','SkillController@destroy');

    //Invitation bid URLs
    Route::any('invite','InvitationController@index');
    Route::any('invite/sendmessage','InvitationController@sendmessage');
    Route::any('invite/getCountContactor','InvitationController@getCountContactor');
    Route::any('invite/report_invitation','InvitationController@report_invitation');
    Route::any('invite/get_datatable_data','InvitationController@get_datatable_data');
    Route::any('invite/view/{invite_id}','InvitationController@show_details');
    Route::get('invite/GetAllContactor','InvitationController@GetAllContactor');
    Route::any('invite/export/{invite_id}','InvitationController@ExportProjectData');
    Route::post('invite/get_contactors','InvitationController@getContractors')->name('invite.get_contactors');
    //Setting
    Route::any('setting','SettingController@index');
    Route::any('setting/create','SettingController@create');
    Route::any('setting/store','SettingController@store');
    Route::any('setting/get_datatable_data','SettingController@get_datatable_data');
    Route::any('setting/{setting_id}/create','SettingController@create');
    Route::any('setting/delete/{id}','SettingController@destroy');

    //Template
    Route::any('template','TemplateController@index');
    Route::any('template/GetAllTemplateData','TemplateController@GetAllTemplateData');
    Route::any('template/create','TemplateController@CreateTemplate');
    Route::any('template/store','TemplateController@StoreTemplate');
    Route::any('template/edit/{id}','TemplateController@EditTemplate');
    Route::any('template/delete/{id}','TemplateController@DelteTemplate');

});
 Route::group(['prefix' => 'bookslot', 'middleware' => ['auth:web,admins']], function () {
    Route::get('/','BookslotController@index')->name('bookslot');
    Route::post('/save_date','BookslotController@save_date')->name('save_date');
    Route::any('/checkemail','BookslotController@checkemail');
    Route::any('/store','BookslotController@store');
    Route::any('/schedule_contractors_list','BookslotController@list')->name('schedule_contractors_list');
    Route::post('/get_datatable_data', 'BookslotController@get_datatable_data')->name('bookslot.get_datatable_data');
    Route::post('/changestatus', 'BookslotController@changestatus')->name('bookslot.changestatus');
    Route::post('/store_model_value', 'BookslotController@store_model_value')->name('bookslot.store_model_value');
    Route::any('/edit/{id}', 'BookslotController@edit')->name('bookslot.edit');
    Route::post('/edit_store', 'BookslotController@edit_store')->name('bookslot.edit_store');
/*Front Routes*/
});

 Route::group(['prefix' => 'scheduler'], function () {
    Route::get('/schedule_select','ScheduleController@schedule_select')->name('schedule_select');
     Route::post('/schedule_select_save','ScheduleController@schedule_select_save')->name('schedule_select_save');
         Route::any('/checkemail','ScheduleController@checkemail');

     Route::post('/store','ScheduleController@store')->name('store');
    
/*Front Routes*/
});