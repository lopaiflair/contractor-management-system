const { mix } = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
   .js('resources/assets/js/app-landing.js', 'public/js/app-landing.js')
   .sourceMaps()

   /*Added by me*/  
   .sass('resources/assets/sass/app.scss', 'public/css')
   /*Added by me*/
   .combine([
       'resources/assets/css/bootstrap.min.css',
       'resources/assets/css/font-awesome.min.css',
       'resources/assets/css/ionicons.min.css',
       'node_modules/admin-lte/dist/css/AdminLTE.min.css',
       'node_modules/admin-lte/dist/css/skins/_all-skins.css',
       'node_modules/icheck/skins/square/blue.css',
       'node_modules/icheck/skins/square/yellow.css',
   ], 'public/css/all.css')
   .combine([
       'resources/assets/css/bootstrap.min.css',
       'resources/assets/css/pratt_landing.min.css'
   ], 'public/css/all-landing.css')
   // PACKAGE (ADMINLTE-LARAVEL) RESOURCES
   //VENDOR RESOURCES
   .copy('node_modules/font-awesome/fonts/*.*','public/fonts/')
   .copy('node_modules/ionicons/dist/fonts/*.*','public/fonts/')
   .copy('node_modules/bootstrap/fonts/*.*','public/fonts/')
   .copy('node_modules/admin-lte/dist/css/skins/*.*','public/css/skins')
   .copy('node_modules/admin-lte/dist/img','public/img')
   .copy('node_modules/admin-lte/plugins','public/plugins')
   .copy('node_modules/icheck/skins/square/blue.png','public/css')
   .copy('node_modules/icheck/skins/square/blue@2x.png','public/css')
   .copy('node_modules/icheck/skins/square/yellow.png','public/css')
   .copy('resources/assets/img/*.*','public/img/')

   /*Added by me*/
   .copy('node_modules/ckeditor/lang','public/ckeditor/lang')
   .copy('node_modules/ckeditor/skins','public/ckeditor/skins')
   .copy('node_modules/ckeditor/adapters','public/ckeditor/adapters')
   .copy('node_modules/ckeditor/plugins','public/ckeditor/plugins')
   .copy('node_modules/ckeditor/config.js','public/ckeditor/')
   .copy('node_modules/ckeditor/styles.js','public/ckeditor/')
   .copy('node_modules/ckeditor/contents.css','public/ckeditor/')

   .styles(['resources/assets/css/admin_custom.css'],'public/css/admin_custom.css')

   .styles(['resources/assets/css/front_custom.css'],'public/css/front_custom.css')
   
   .scripts([
      'resources/assets/js/custom.js',
    ], 'public/js/custom.js')

   .scripts([
      'node_modules/jquery-validation/dist/jquery.validate.min.js',
      'node_modules/jquery-validation/dist/additional-methods.min.js',
      'https://getbootstrap.com/assets/js/vendor/popper.min.js',
      //'node_modules/bootstrap/dist/js/bootstrap.js',
    ], 'public/js/other_scripts.js')

   /*Front*/
   .js('resources/assets/js/front_app.js', 'public/js/front_app.js')
   .sass('resources/assets/sass/front_app.scss', 'public/css/front_app.css');
   /*Front*/
   /*Added by me*/

if (mix.config.inProduction) {
  mix.version();
  mix.minify();
}

