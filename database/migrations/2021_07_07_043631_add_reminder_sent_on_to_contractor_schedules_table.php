<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddReminderSentOnToContractorSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractor_schedules', function (Blueprint $table) {
            $table->timestamp('reminder_sent_on')->after('reminder_interval');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractor_schedules', function (Blueprint $table) {
            $table->dropColumn('reminder_sent_on');
        });
    }
}
