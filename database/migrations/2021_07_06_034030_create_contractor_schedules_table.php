<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractor_schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->date('schedule_date');
            $table->string('timeslot')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('what_address_you_are_coming_to')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('skill_set')->nullable();
            $table->string('sent_text_message_to')->nullable();
            $table->text('schedule_message')->nullable();
            $table->enum('schedule_message_sent', ['yes', 'no'])->default('no');
            $table->enum('hired', ['yes', 'no'])->default('no');
            $table->enum('reminder', ['on', 'off'])->default('off');
            $table->string('reminder_message')->nullable();
            $table->enum('reminder_interval', ['hourly', 'daily']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractor_schedules');
    }
}
