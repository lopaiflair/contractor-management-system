<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInviteSendTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invite_send', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('invite_id')->nullable();
            $table->integer('contractor_id')->nullable();
            $table->integer('mobile_no')->nullable();
            $table->string('email')->nullable();
            $table->integer('message_sent')->nullable();
            $table->integer('email_sent')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invite_send');
    }
}
