<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->increments('id');
            $table->text('email_content')->nullable();
            $table->text('sms_content')->nullable();
            $table->text('ph_numbers_to_send_notification')->nullable();
            $table->enum('report_notification', ['On', 'Off'])->default('On')->nullable();
            $table->string('send_email_after_days')->nullable();
            $table->string('send_email_after_hours')->nullable();
            $table->string('send_email_after_minutes')->nullable();
            $table->text('report_notification_content')->nullable();
            $table->string('send_sms_after_days')->nullable();
            $table->string('send_sms_after_hours')->nullable();
            $table->string('send_sms_after_minutes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
