<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('phone_no')->nullable();
            $table->string('email')->nullable();
            $table->text('linkedin_handler')->nullable();
            $table->text('twitter_handler')->nullable();
            $table->text('facebook_handler')->nullable();
            $table->text('from_enrich')->nullable();
            $table->enum('sms', ['Sent', 'Not Yet', 'Failed'])->default('Not Yet');
            $table->enum('mail', ['Sent', 'Not Yet', 'Failed'])->default('Not Yet');
            $table->dateTime('added_on')->nullable();
            $table->dateTime('email_sent_on')->nullable();
            $table->dateTime('sms_sent_on')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('addresses', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
            $table->dropColumn('phone_no');
            $table->dropColumn('email');
            $table->dropColumn('linkedin_handler');
            $table->dropColumn('twitter_handler');
            $table->dropColumn('facebook_handler');
            $table->dropColumn('sms');
            $table->dropColumn('mail');
            $table->dropColumn('added_on');
        });
    }
}
