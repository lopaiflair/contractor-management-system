<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSchedulerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduler', function (Blueprint $table) {
            $table->increments('id');
            $table->date('schedule_date');
            $table->string('timeslot')->nullable();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('what_address_you_are_coming_to')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('skill_set')->nullable();
            $table->datetime('reminder_sent_on')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduler');
    }
}
