<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert(
            [
                [
                    'name' => "Admin",
                    'email' => "admin@mailinator.com",
                    'password' => bcrypt('123456'),
                    'role' => 'Admin',
                ],
                [
                    'name' => "Ritesh",
                    'email' => "ritesh@mailinator.com",
                    'password' => bcrypt('123456'),
                    'role' => 'User',
                ],
            ]
        );
    }
}
